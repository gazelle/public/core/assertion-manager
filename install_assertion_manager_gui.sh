#!/bin/bash

DATABASE="assertion-manager-gui,assertion-manager"
PACKAGE="AssertionManagerGui.ear"
JENKINS_JOB="AssertionManager-Gui-RELEASE"
EAR_FOLDER="AssertionManagerGui-ear"

wget http://gazelle.ihe.net/jenkins/job/Installer_script/ws/_install.sh -O _install.sh
chmod +x _install.sh

./_install.sh "$DATABASE" "$PACKAGE" "$JENKINS_JOB" "$EAR_FOLDER"
