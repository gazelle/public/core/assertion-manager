#! /bin/bash

EAR="AssertionManagerGui.ear"

FILE=`ls packages_backup/"$EAR"* -Atr|tail -1`

read -p "restore $FILE? [y,n]" 

if [ "$REPLY" = "y" ] || "$REPLY" = "Y"]; then 
  cp $FILE /usr/local/jboss/server/gazelle/deploy/$EAR
fi
