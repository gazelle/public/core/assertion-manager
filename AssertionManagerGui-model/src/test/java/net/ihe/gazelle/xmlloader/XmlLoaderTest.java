package net.ihe.gazelle.xmlloader;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.OasisTargetQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertionQuery;
import net.ihe.gazelle.oasis.xmlloader.Listener;
import net.ihe.gazelle.oasis.xmlloader.XmlLoader;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.core.ResourceLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@Ignore
public class XmlLoaderTest extends JunitTestQueryJunit4 {

    private String[] sectionsTab = {"/KSP.xml"};

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        cleanData();
        super.tearDown();
    }

    private void cleanData() {
        OasisTestAssertionFactory.cleanOasisTestAssertions();
    }

    @Override
    protected String getDb() {
        return "assertion-manager-junit";
    }

    @Override
    protected String getHbm2ddl() {
        return "update";
    }

    @Test
    public void test_loadXml() throws IOException {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        XmlLoader xmlLoader = new XmlLoader(entityManager, new Listener() {
            @Override
            public void send(String message) {

            }

            @Override
            public void sendFinalMessage(String message) {

            }
        }, "admin");
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }

        for (int i = 0; i < sectionsTab.length; i++) {

            URL xmlurl = ResourceLoader.class.getResource(sectionsTab[i]);
            String xml = FileUtils.readFileToString(new File(xmlurl.getFile()));

            try {
                xmlLoader.load(xml);
                System.out.println(sectionsTab[i] + " end");
                System.out.println("Update");
                xmlLoader.load(xml);
                System.out.println(sectionsTab[i] + " end");
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            }
        }
        entityManager.flush();
        entityManager.getTransaction().commit();

        OasisTargetQuery query = new OasisTargetQuery();
        assertEquals(1, query.getCount());

        OasisTestAssertionQuery assertionQuery = new OasisTestAssertionQuery();
        assertEquals(28, assertionQuery.getCount());

    }
}
