package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTextSourceItemFactory;
import net.ihe.gazelle.oasis.testassertion.OasisTextSourceItem;
import net.ihe.gazelle.oasis.testassertion.OasisTextSourceItemQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisTextSourceItemTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public int get_TextSourceItem_Table_Size() {
        OasisTextSourceItemQuery query = new OasisTextSourceItemQuery();
        return query.getList().size();
    }

    @Test
    public void test_textSourceItem_works_without_content() {
        int initSize = get_TextSourceItem_Table_Size();

        OasisTextSourceItem textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();
        textSourceItem.setContent(null);

        DatabaseManager.writeObject(textSourceItem);

        int endSize = get_TextSourceItem_Table_Size();
        assertEquals("No assertion should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_textSourceItem_works_without_name() {
        int initSize = get_TextSourceItem_Table_Size();

        OasisTextSourceItem textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();
        textSourceItem.setName(null);

        DatabaseManager.writeObject(textSourceItem);

        int endSize = get_TextSourceItem_Table_Size();
        assertEquals("No assertion should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_textSourceItem_works_with_empty_name_and_content() {
        int initSize = get_TextSourceItem_Table_Size();

        OasisTextSourceItem textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();

        DatabaseManager.writeObject(textSourceItem);

        int endSize = get_TextSourceItem_Table_Size();
        assertEquals("No assertion should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_textSourceItem_works_with_260_char_long_content() {
        int initSize = get_TextSourceItem_Table_Size();

        OasisTextSourceItem textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();
        textSourceItem.setContent(RandomStringUtils.randomAscii(260));

        DatabaseManager.writeObject(textSourceItem);

        int endSize = get_TextSourceItem_Table_Size();
        assertEquals("No assertion should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_textSourceItem_works_with_255_char_long_name() {
        int initSize = get_TextSourceItem_Table_Size();

        OasisTextSourceItem textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();
        textSourceItem.setName(RandomStringUtils.randomAscii(255));

        DatabaseManager.writeObject(textSourceItem);

        int endSize = get_TextSourceItem_Table_Size();
        assertEquals("No assertion should be stored", 1, endSize - initSize);
    }
}
