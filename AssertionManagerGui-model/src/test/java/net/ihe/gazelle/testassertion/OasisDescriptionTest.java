package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisDescriptionFactory;
import net.ihe.gazelle.oasis.testassertion.OasisDescription;
import net.ihe.gazelle.oasis.testassertion.OasisDescriptionQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisDescriptionTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public int get_description_Table_Size() {
        OasisDescriptionQuery query = new OasisDescriptionQuery();
        return query.getList().size();
    }
    @Test
    public void test_description_has_a_valid_factory() {
        int initSize = get_description_Table_Size();

        OasisDescription description = OasisDescriptionFactory.createDescriptionWithMandatoryFields();
        DatabaseManager.writeObject(description);

        int endSize = get_description_Table_Size();
        assertEquals("1 description should be stored", 1, endSize - initSize);
    }
    @Test
    public void test_OasisDescription_works_with_300_char_long_content() {
        int initSize = get_description_Table_Size();

        OasisDescription description = OasisDescriptionFactory.createDescriptionWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        description.setContent(randomAscii);

        OasisDescription obj = (OasisDescription) DatabaseManager.writeObject(description);

        int endSize = get_description_Table_Size();
        assertEquals("1 new description should be stored", 1, endSize - initSize);

        OasisDescription result = OasisDescriptionFactory.getOasisDescriptionFromDatabase(obj);
        assertEquals(randomAscii, result.getContent());
    }
}
