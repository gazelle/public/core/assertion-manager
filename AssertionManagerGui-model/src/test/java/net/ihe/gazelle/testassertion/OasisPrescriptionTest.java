package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisPrescriptionFactory;
import net.ihe.gazelle.oasis.testassertion.OasisPrescription;
import net.ihe.gazelle.oasis.testassertion.OasisPrescriptionQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;
@Ignore
public class OasisPrescriptionTest extends JunitTestQueryJunit4 {

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private int get_OasisPrescription_Table_Size() {
        OasisPrescriptionQuery query = new OasisPrescriptionQuery();
        return query.getList().size();
    }

    @Test
    public void test_OasisPrescription_has_a_valid_factory() {
        int initSize = get_OasisPrescription_Table_Size();

        OasisPrescription prescription = OasisPrescriptionFactory.createPrescriptionWithMandatoryFieldsAndSaveIt();
        prescription = (OasisPrescription) DatabaseManager.writeObject(prescription);
        assertNotNull("Prescription should have been stored but wasn't", prescription.getId());

        int endSize = get_OasisPrescription_Table_Size();
        assertEquals("1 prescription should be stored", 1, endSize - initSize);
        assertEquals(OasisPrescriptionFactory.getDefaultLevel(), prescription.getLevel());
    }

    @Test
    public void test_OasisPrescription_works_without_content() {
        int initSize = get_OasisPrescription_Table_Size();

        OasisPrescription prescription = OasisPrescriptionFactory.createPrescriptionWithMandatoryFields();
        prescription.setContent(null);
        prescription = (OasisPrescription) DatabaseManager.writeObject(prescription);
        assertNotNull(prescription.getId());

        int endSize = get_OasisPrescription_Table_Size();
        assertEquals("one prescription should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisPrescription_fails_without_level() {
        int initSize = get_OasisPrescription_Table_Size();

        OasisPrescription prescription = OasisPrescriptionFactory.createPrescriptionWithMandatoryFields();
        prescription.setLevel(null);
        prescription = (OasisPrescription) DatabaseManager.writeObject(prescription);
        assertNull(prescription.getId());

        int endSize = get_OasisPrescription_Table_Size();
        assertEquals("No prescription should be stored", 0, endSize - initSize);
    }

    @Test
    public void test_OasisPrescription_works_with_300_char_long_content() {
        int initSize = get_OasisPrescription_Table_Size();

        OasisPrescription prescription = OasisPrescriptionFactory.createPrescriptionWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(255);
        prescription.setContent(randomAscii);

        prescription = (OasisPrescription) DatabaseManager.writeObject(prescription);

        int endSize = get_OasisPrescription_Table_Size();
        assertEquals("1 new prescription should be stored", 1, endSize - initSize);
        assertNotNull(prescription.getId());

        OasisPrescription result = OasisPrescriptionFactory.getOasisPrescriptionFromDatabase(prescription);
        assertEquals(randomAscii, result.getContent());
    }

}
