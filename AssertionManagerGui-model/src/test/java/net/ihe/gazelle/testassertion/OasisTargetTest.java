package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTargetFactory;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import net.ihe.gazelle.oasis.testassertion.OasisTargetQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@Ignore
public class OasisTargetTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private int get_Oasis_Target_Table_Size() {
        OasisTargetQuery query = new OasisTargetQuery();
        return query.getList().size();
    }

    @Test
    public void test_OasisTarget_has_a_valid_factory() {
        int initSize = get_Oasis_Target_Table_Size();

        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        DatabaseManager.writeObject(target);

        int endSize = get_Oasis_Target_Table_Size();
        assertEquals("1 target should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisTarget_works_without_content() {
        int initSize = get_Oasis_Target_Table_Size();

        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        target.setContent(null);
        DatabaseManager.writeObject(target);

        int endSize = get_Oasis_Target_Table_Size();
        assertEquals("1 target should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisTarget_works_without_type() {
        int initSize = get_Oasis_Target_Table_Size();

        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        target.setType(null);
        DatabaseManager.writeObject(target);

        int endSize = get_Oasis_Target_Table_Size();
        assertEquals("1 target should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisTarget_works_without_idScheme() {
        int initSize = get_Oasis_Target_Table_Size();

        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        target.setIdScheme(null);
        DatabaseManager.writeObject(target);

        int endSize = get_Oasis_Target_Table_Size();
        assertEquals("1 target should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisTarget_works_without_language() {
        int initSize = get_Oasis_Target_Table_Size();

        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        target.setLanguage(null);
        DatabaseManager.writeObject(target);

        int endSize = get_Oasis_Target_Table_Size();
        assertEquals("1 target should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisTarget_getDocuments() {
        int initSize = get_Oasis_Target_Table_Size();

        OasisTarget target = OasisTargetFactory.createTargetWithMandatoryFields();
        target.setLanguage(null);
        DatabaseManager.writeObject(target);

        int endSize = get_Oasis_Target_Table_Size();
        assertEquals("1 target should be stored", 1, endSize - initSize);
    }
}
