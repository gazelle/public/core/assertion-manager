package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.*;
import net.ihe.gazelle.oasis.testassertion.*;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
@Ignore
public class OasisNormativeSourceTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    public int get_normativeSource_Table_Size() {
        OasisNormativeSourceQuery query = new OasisNormativeSourceQuery();
        return query.getList().size();
    }

    @Test
    public void test_normativeSource_has_a_valid_factory() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_normativeSource_is_valid_with_260_char_long_content() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        String content = RandomStringUtils.randomAscii(260);
        normativeSource.setContent(content);
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertEquals(content, normativeSource.getContent());
    }

    @Test
    public void test_normativeSource_is_valid_without_a_comment() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        normativeSource.setComment(null);
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertNull(normativeSource.getComment());
    }

    @Test
    public void test_normativeSource_is_valid_with_a_comment() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        OasisComment comment = OasisCommentFactory.createCommentWithMandatoryFields();
        normativeSource.setComment(comment);
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertNotNull(normativeSource.getComment());
    }

    @Test
    public void test_normativeSource_is_valid_without_an_interpretation() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        normativeSource.setInterpretation(null);
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertNull(normativeSource.getInterpretation());
    }

    @Test
    public void test_normativeSource_is_valid_with_an_interpretation() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        OasisInterpretation interpretation = OasisInterpretationFactory.createInterpretationWithMandatoryFields();
        normativeSource.setInterpretation(interpretation);
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertNotNull(normativeSource.getInterpretation());
    }

    @Test
    public void test_normativeSource_is_valid_without_content() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        normativeSource.setContent(null);
        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_normativeSource_is_valid_with_a_derived_source_item() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        OasisDerivedSourceItem derivedSourceItem = OasisDerivedSourceItemFactory
                .createDerivedSourceItemWithMandatoryFields();

        List<OasisDerivedSourceItem> derivedSourceItems = new ArrayList<OasisDerivedSourceItem>();
        derivedSourceItems.add(derivedSourceItem);

        normativeSource.setDerivedSourceItems(derivedSourceItems);

        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertEquals(1, normativeSource.getDerivedSourceItems().size());
    }

    @Test
    public void test_normativeSource_is_valid_with_ten_derived_source_items() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();

        OasisDerivedSourceItem derivedSourceItem;
        List<OasisDerivedSourceItem> derivedSourceItems = new ArrayList<OasisDerivedSourceItem>();

        for (int i = 1; i <= 10; i++) {
            derivedSourceItem = OasisDerivedSourceItemFactory.createDerivedSourceItemWithMandatoryFields();
            derivedSourceItems.add(derivedSourceItem);
        }

        normativeSource.setDerivedSourceItems(derivedSourceItems);

        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertEquals(10, normativeSource.getDerivedSourceItems().size());
    }

    @Test
    public void test_normativeSource_is_valid_with_a_text_source_item() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        OasisTextSourceItem textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();

        List<OasisTextSourceItem> textSourceItems = new ArrayList<OasisTextSourceItem>();
        textSourceItems.add(textSourceItem);

        normativeSource.setTextSourceItems(textSourceItems);

        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertEquals(1, normativeSource.getTextSourceItems().size());
    }

    @Test
    public void test_normativeSource_is_valid_with_ten_text_source_items() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();

        OasisTextSourceItem textSourceItem;
        List<OasisTextSourceItem> textSourceItems = new ArrayList<OasisTextSourceItem>();

        for (int i = 1; i <= 10; i++) {
            textSourceItem = OasisTextSourceItemFactory.createTextSourceItemWithMandatoryFields();
            textSourceItems.add(textSourceItem);
        }

        normativeSource.setTextSourceItems(textSourceItems);

        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertEquals(10, normativeSource.getTextSourceItems().size());
    }

    @Test
    public void test_normativeSource_is_valid_with_a_ref_source_item() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();
        OasisRefSourceItem refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();

        List<OasisRefSourceItem> refSourceItems = new ArrayList<OasisRefSourceItem>();
        refSourceItems.add(refSourceItem);

        normativeSource.setRefSourceItems(refSourceItems);

        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertEquals(1, normativeSource.getRefSourceItems().size());
    }

    @Test
    public void test_normativeSource_is_valid_with_ten_ref_source_items() {
        int initSize = get_normativeSource_Table_Size();

        OasisNormativeSource normativeSource = OasisNormativeSourceFactory.createNormativeSourceWithMandatoryFields();

        OasisRefSourceItem refSourceItem;
        List<OasisRefSourceItem> refSourceItems = new ArrayList<OasisRefSourceItem>();

        for (int i = 1; i <= 10; i++) {
            refSourceItem = OasisRefSourceItemFactory.createRefSourceItemWithMandatoryFields();
            refSourceItems.add(refSourceItem);
        }

        normativeSource.setRefSourceItems(refSourceItems);

        DatabaseManager.writeObject(normativeSource);

        int endSize = get_normativeSource_Table_Size();
        assertEquals("1 normative source should be stored", 1, endSize - initSize);
        assertEquals(10, normativeSource.getRefSourceItems().size());
    }
}
