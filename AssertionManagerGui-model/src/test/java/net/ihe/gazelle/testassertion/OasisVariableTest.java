package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisVariableFactory;
import net.ihe.gazelle.oasis.testassertion.OasisLanguageValues;
import net.ihe.gazelle.oasis.testassertion.OasisVariable;
import net.ihe.gazelle.oasis.testassertion.OasisVariableQuery;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
@Ignore
public class OasisVariableTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private int get_OasisVariable_Table_Size() {
        OasisVariableQuery query = new OasisVariableQuery();
        return query.getList().size();
    }

    @Test
    public void test_OasisVariable_has_a_valid_factory() {
        int initSize = get_OasisVariable_Table_Size();

        OasisVariable variable = OasisVariableFactory.createVariableWithMandatoryFieldsAndSaveIt();
        DatabaseManager.writeObject(variable);

        int endSize = get_OasisVariable_Table_Size();
        assertEquals("1 variable should be stored", 1, endSize - initSize);
    }

    @Test
    public void test_OasisVariable_fails_without_Name() {
        int initSize = get_OasisVariable_Table_Size();

        OasisVariable variable = OasisVariableFactory.createVariableWithMandatoryFields();
        variable.setName(null);

        DatabaseManager.writeObject(variable);

        int endSize = get_OasisVariable_Table_Size();
        assertEquals("No variable should be stored", 0, endSize - initSize);
    }

    @Test
    public void test_OasisVariable_works_with_300_char_long_content() {
        int initSize = get_OasisVariable_Table_Size();

        OasisVariable variable = OasisVariableFactory.createVariableWithMandatoryFields();
        String randomAscii = RandomStringUtils.randomAscii(300);
        variable.setContent(randomAscii);

        OasisVariable obj = (OasisVariable) DatabaseManager.writeObject(variable);

        int endSize = get_OasisVariable_Table_Size();
        assertEquals("1 variable should be stored", 1, endSize - initSize);

        OasisVariable result = getOasisVariableFromDatabase(obj);
        assertNotNull(result);
        assertEquals(randomAscii, result.getContent());
    }

    @Test
    public void test_OasisVariable_works_with_255_char_long_language() {
        int initSize = get_OasisVariable_Table_Size();

        OasisVariable variable = OasisVariableFactory.createVariableWithMandatoryFields();
        variable.setLanguage(OasisLanguageValues.NATURAL);

        OasisVariable obj = (OasisVariable) DatabaseManager.writeObject(variable);

        int endSize = get_OasisVariable_Table_Size();
        assertEquals("1 variable should be stored", 1, endSize - initSize);

        OasisVariable result = getOasisVariableFromDatabase(obj);
        assertNotNull(result);
        assertEquals(OasisLanguageValues.NATURAL, result.getLanguage());
    }

    private OasisVariable getOasisVariableFromDatabase(OasisVariable variable) {
        if (variable.getId() == null) {
            return null;
        } else {
            OasisVariableQuery query = new OasisVariableQuery();
            query.eq(variable);
            List<OasisVariable> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }

}
