package net.ihe.gazelle.testassertion;

import net.ihe.gazelle.assertion.coverage.factories.AssertionLinkedToTestFactory;
import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertionQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
@Ignore
public class OasisTestAssertionTest extends JunitTestQueryJunit4 {

    @Before
    public void setUp() throws Exception {
        super.setUp();
        EntityManagerService.provideEntityManager().getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
        cleanData();

        EntityManagerService.provideEntityManager().close();
        super.tearDown();
    }

    private void cleanData() {
        AssertionLinkedToTestFactory.cleanAssertionLinkedToTest();
        OasisTestAssertionFactory.cleanOasisTestAssertions();
    }

    public int get_Test_Assertion_Table_Size() {
        OasisTestAssertionQuery query = new OasisTestAssertionQuery();
        return query.getList().size();
    }

    @Test
    public void test_testAssertion_has_a_valid_factory() {
        int initSize = get_Test_Assertion_Table_Size();

        OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);

        int endSize = get_Test_Assertion_Table_Size();
        assertEquals("1 assertion should be stored", 1, endSize - initSize);
        assertNotNull(assertion.getId());
        assertNotNull(assertion.getAssertionId());
        assertNotNull(assertion.getNormativeSource());
        assertNotNull(assertion.getTarget());
        assertNotNull(assertion.getPredicate());
    }

    //
    //	public void test_testAssertion_fails_without_predicate() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //
    //		assertion.setPredicate(null);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("No assertion should be stored", 0, endSize - initSize);
    //		assertNull(assertion.getPredicate());
    //	}
    //
    //	public void test_testAssertion_fails_without_target() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //
    //		assertion.setTarget(null);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("No assertion should be stored", 0, endSize - initSize);
    //		assertNull(assertion.getTarget());
    //	}
    //
    //	public void test_testAssertion_fails_without_normative_source() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		assertion.setNormativeSource(null);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("No assertion should be stored", 0, endSize - initSize);
    //	}
    //
    //	public void test_testAssertion_has_a_normative_source() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNotNull(assertion.getNormativeSource());
    //	}
    //
    //	public void test_testAssertion_is_valid_without_prescription_level() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		assertion.setPrescription(null);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNull(assertion.getPrescription());
    //	}
    //
    //	public void test_testAssertion_is_valid_with_prescription_level() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		OasisPrescription prescription = OasisPrescriptionFactory.createPrescriptionWithMandatoryFieldsAndSaveIt();
    //		assertion.setPrescription(prescription);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNotNull(assertion.getPrescription());
    //	}
    //
    //	public void test_testAssertion_is_valid_without_prerequisite() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		assertion.setPrerequisite(null);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNull(assertion.getPrerequisite());
    //	}
    //
    //	public void test_testAssertion_is_valid_with_a_prerequisite() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		OasisPrerequisite prerequisite = OasisPrerequisiteFactory.createPrerequisiteWithMandatoryFieldsAndSaveIt();
    //		assertion.setPrerequisite(prerequisite);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNotNull(assertion.getPrerequisite());
    //	}
    //
    //	public void test_testAssertion_is_valid_without_description() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		assertion.setDescription(null);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNull(assertion.getDescription());
    //	}
    //
    //	public void test_testAssertion_is_valid_with_description() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		OasisDescription description = OasisDescriptionFactory.createDescriptionWithMandatoryFields();
    //		assertion.setDescription(description);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNotNull(assertion.getDescription());
    //	}
    //
    //	public void test_testAssertion_is_valid_without_tag() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		assertion.setTags(null);
    //		assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertNull(assertion.getTags());
    //	}
    //
    //	public void test_testAssertion_is_valid_with_one_tag() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		OasisTag tag = OasisTagFactory.createTagWithMandatoryFieldsAndSaveIt();
    //		List<OasisTag> tags = new ArrayList<OasisTag>();
    //		tags.add(tag);
    //
    //		assertion.setTags(tags);
    //		assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertEquals(1, assertion.getTags().size());
    //	}
    //
    //	public void test_testAssertion_is_valid_with_ten_tags() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //
    //		OasisTag tag;
    //		List<OasisTag> tags = new ArrayList<OasisTag>();
    //		for (int i = 1; i <= 10; i++) {
    //			tag = OasisTagFactory.createTagWithMandatoryFieldsAndSaveIt();
    //			tags.add(tag);
    //		}
    //
    //		assertion.setTags(tags);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertEquals(10, assertion.getTags().size());
    //	}
    //
    //	public void test_testAssertion_is_valid_without_variable() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		assertion.setVariables(null);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //	}
    //
    //	public void test_testAssertion_is_valid_with_one_variable() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		OasisVariable variable = OasisVariableFactory.createVariableWithMandatoryFieldsAndSaveIt();
    //		List<OasisVariable> variables = new ArrayList<OasisVariable>();
    //		variables.add(variable);
    //
    //		assertion.setVariables(variables);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //		assertEquals(1, assertion.getVariables().size());
    //	}
    //
    //	public void test_testAssertion_is_valid_with_ten_variables() {
    //		int initSize = get_Test_Assertion_Table_Size();
    //
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //
    //		List<OasisVariable> variables = new ArrayList<OasisVariable>();
    //		OasisVariable variable;
    //		for (int i = 1; i <= 10; i++) {
    //			variable = OasisVariableFactory.createVariableWithMandatoryFieldsAndSaveIt();
    //			variables.add(variable);
    //		}
    //
    //		assertion.setVariables(variables);
    //		DatabaseManager.writeObject(assertion);
    //
    //		int endSize = get_Test_Assertion_Table_Size();
    //		assertEquals("1 assertion should be stored", 1, endSize - initSize);
    //
    //		assertEquals(10, assertion.getVariables().size());
    //	}

    //	@Test
    //	@Ignore
    //	public void test_assertion_linked_to_tests() {
    //		OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
    //		assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);
    //
    //		OasisTestAssertionQuery query = new OasisTestAssertionQuery();
    //		query.assertionId().eq(assertion.getAssertionId());
    //		query.target().idScheme().eq(assertion.getTarget().getIdScheme());
    //		OasisTestAssertion uniqueResult = query.getUniqueResult();
    //		assertEquals(0, uniqueResult.getTests().size());
    //
    //		AssertionLinkedToTest testLink = AssertionLinkedToTestFactory
    //				.createAssertionLinkedToTestWithMandatoryFields(assertion);
    //
    //		OasisTestAssertionQuery query2 = new OasisTestAssertionQuery();
    //		query2.assertionId().eq(assertion.getAssertionId());
    //		query2.target().idScheme().eq(assertion.getTarget().getIdScheme());
    //		OasisTestAssertion uniqueResult2 = query2.getUniqueResult();
    //		assertEquals(1, testLink.getAssertion().getTests().size());
    //	}

    //	@Test
    //	public void test_assertion_linked_to_testSteps() {
    //		OasisTestAssertionQuery query = new OasisTestAssertionQuery();
    //		query.assertionId().eq("CONF-20");
    //		query.target().idScheme().eq("DIS");
    //		OasisTestAssertion uniqueResult = query.getUniqueResult();
    //		assertEquals(5, uniqueResult.getTestSteps().size());
    //
    //		query = new OasisTestAssertionQuery();
    //		query.assertionId().eq("CONF-20");
    //		query.target().idScheme().eq("DIS");
    //		uniqueResult = query.getUniqueResult();
    //		assertEquals(4, uniqueResult.getTests().size());
    //
    //		query = new OasisTestAssertionQuery();
    //		query.assertionId().eq("CONF-20");
    //		query.target().idScheme().eq("DIS");
    //		uniqueResult = query.getUniqueResult();
    //		assertEquals(1, uniqueResult.getTfRules().size());
    //
    //		query = new OasisTestAssertionQuery();
    //		query.assertionId().eq("CONF-20");
    //		query.target().idScheme().eq("DIS");
    //		uniqueResult = query.getUniqueResult();
    //		uniqueResult.updateCoverage();
    //		assertEquals(9, uniqueResult.getTestsCoverageCount().intValue());
    //	}
}
