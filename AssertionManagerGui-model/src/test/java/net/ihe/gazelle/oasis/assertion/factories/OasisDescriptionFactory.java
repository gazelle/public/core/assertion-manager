package net.ihe.gazelle.oasis.assertion.factories;

import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.testassertion.OasisDescription;
import net.ihe.gazelle.oasis.testassertion.OasisDescriptionQuery;

import java.util.List;

public class OasisDescriptionFactory {
    public static OasisDescription createDescriptionWithMandatoryFields() {
        OasisDescription description = new OasisDescription();
        description = (OasisDescription) DatabaseManager.writeObject(description);
        return description;
    }

    public static void cleanOasisDescription() {
        OasisDescriptionQuery descriptionQuery = new OasisDescriptionQuery();
        List<OasisDescription> descriptions = descriptionQuery.getListDistinct();
        for (OasisDescription description : descriptions) {
            DatabaseManager.removeObject(description);
        }
    }

    public static OasisDescription getOasisDescriptionFromDatabase(OasisDescription description) {
        if (description.getId() == null) {
            return null;
        } else {
            OasisDescriptionQuery query = new OasisDescriptionQuery();
            query.eq(description);
            List<OasisDescription> results = query.getList();

            if (results.size() == 0) {
                return null;
            } else {
                return results.get(0);
            }
        }
    }
}
