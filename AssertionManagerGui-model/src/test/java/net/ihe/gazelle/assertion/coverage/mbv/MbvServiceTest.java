package net.ihe.gazelle.assertion.coverage.mbv;

import net.ihe.gazelle.assertion.coverage.mbv.factories.MbvServiceFactory;
import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.MbvCoverageStatus;
import net.ihe.gazelle.oasis.testassertion.MbvService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.util.ArrayList;
import java.util.List;
@Ignore
public class MbvServiceTest extends JunitTestQueryJunit4 {
    @Before
    public void setUp() throws Exception {
        super.setUp();
        EntityManager em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();
        em.setFlushMode(FlushModeType.COMMIT);
    }

    @After
    public void tearDown() throws Exception {
        cleanData();
        super.tearDown();
    }

    private void cleanData() {
        MbvServiceFactory.cleanMbvService();
    }

    @Override
    protected String getDb() {
        return "assertion-manager-junit";
    }

    @Override
    protected String getHbm2ddl() {
        return "update";
    }

    protected boolean getShowSql() {
        return true;
    }

    @Test
    public void createService() {
        MbvService mbvService = MbvServiceFactory.createMbvServiceWithMandatoryFields();

        EntityManager em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();
        MbvCoverageStatus cov = new MbvCoverageStatus(MbvCoverageStatus.CoverageStatusEnum.Covered, "idScheme", "assertionId", mbvService);
        List<MbvCoverageStatus> coverages = new ArrayList<MbvCoverageStatus>();
        coverages.add(cov);
        mbvService.setCoverageStatus(coverages);
        em.merge(mbvService);
        em.flush();

    }

}
