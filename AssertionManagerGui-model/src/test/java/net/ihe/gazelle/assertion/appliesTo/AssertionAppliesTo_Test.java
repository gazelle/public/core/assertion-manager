package net.ihe.gazelle.assertion.appliesTo;

import net.ihe.gazelle.assertion.appliesTo.factories.AssertionAppliesToActorFactory;
import net.ihe.gazelle.assertion.appliesTo.factories.AssertionAppliesToAipoFactory;
import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.util.List;

import static org.junit.Assert.assertEquals;
@Ignore
public class AssertionAppliesTo_Test extends JunitTestQueryJunit4 {

    private EntityManager em;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();

        em.setFlushMode(FlushModeType.COMMIT);
    }

    @After
    public void tearDown() throws Exception {
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        em.flush();
         cleanData();
        super.tearDown();
    }

    private void cleanData() {
        AssertionAppliesToActorFactory.cleanAssertionAppliesToActor();
        AssertionAppliesToAipoFactory.cleanAssertionAppliesToAipo();
        OasisTestAssertionFactory.cleanOasisTestAssertions();
    }

    @Test
    public void AssertionsCouldBeLinkedToActors_aipo() {
        AssertionAppliesToActor actorLink = new AssertionAppliesToActor();
        actorLink.setAssertion(OasisTestAssertionFactory.createTestAssertionWithMandatoryFields());
        actorLink.setLinkedEntityKeyword("ACTOR_KEY");
        actorLink.setLinkedEntityId(1);
        actorLink.setLinkedEntityName("actor name");
        actorLink.setLinkedEntityPermalink("http://gazelle.ihe.net");
        actorLink.setLinkedEntityProvider("tm");
        actorLink = (AssertionAppliesToActor) DatabaseManager.writeObject(actorLink);

        AssertionAppliesToAipo aipoLink = new AssertionAppliesToAipo();
        aipoLink.setAssertion(OasisTestAssertionFactory.createTestAssertionWithMandatoryFields());
        aipoLink.setLinkedEntityKeyword("AIPO_KEY");
        aipoLink.setLinkedEntityId(1);
        aipoLink.setLinkedEntityName("aipo name");
        aipoLink.setLinkedEntityPermalink("http://gazelle.ihe.net");
        aipoLink.setLinkedEntityProvider("tm");
        aipoLink = (AssertionAppliesToAipo) DatabaseManager.writeObject(aipoLink);

        AssertionAppliesToActorQuery queryActor = new AssertionAppliesToActorQuery();
        List<AssertionAppliesToActor> appliesToActors = queryActor.getListDistinct();
        assertEquals(1, appliesToActors.size());

        AssertionAppliesToAipoQuery queryAipo = new AssertionAppliesToAipoQuery();
        List<AssertionAppliesToAipo> appliesToAIPO = queryAipo.getListDistinct();
        assertEquals(1, appliesToAIPO.size());

        AssertionAppliesToQuery query = new AssertionAppliesToQuery();
        List<AssertionAppliesTo> appliesTo = query.getListDistinct();
        assertEquals(2, appliesTo.size());

        AssertionAppliesToQuery query2 = new AssertionAppliesToQuery();
        query2.linkedEntity().eq(AssertionAppliesToActor.DB_DISCRIMINATOR);
        List<AssertionAppliesTo> appliesToActor = query2.getListDistinct();
        assertEquals(1, appliesToActor.size());
        assertEquals("actor name", appliesToActor.get(0).getLinkedEntityName());

        AssertionAppliesToQuery query3 = new AssertionAppliesToQuery();
        query3.linkedEntity().eq(AssertionAppliesToAipo.DB_DISCRIMINATOR);
        List<AssertionAppliesTo> appliesToAipo = query3.getListDistinct();
        assertEquals(1, appliesToAipo.size());
        assertEquals("aipo name", appliesToAipo.get(0).getLinkedEntityName());
    }

}
