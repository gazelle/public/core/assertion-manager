package net.ihe.gazelle.assertion.scope;

import net.ihe.gazelle.assertion.junit.JunitTestQueryJunit4;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.DatabaseManager;
import net.ihe.gazelle.oasis.assertion.factories.OasisTestAssertionFactory;
import net.ihe.gazelle.oasis.testassertion.AssertionScope;
import net.ihe.gazelle.oasis.testassertion.AssertionScopeQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
@Ignore
public class AssertionScopeTest extends JunitTestQueryJunit4 {

    @Before
    public void setUp() throws Exception {
        super.setUp();
        EntityManager em = EntityManagerService.provideEntityManager();
        em.getTransaction().begin();
        cleanData();
        em.setFlushMode(FlushModeType.COMMIT);
    }

    @After
    public void tearDown() throws Exception {
        //cleanData();
        super.tearDown();
    }

    private void cleanData() {

        AssertionScopeQuery query = new AssertionScopeQuery(EntityManagerService.provideEntityManager());
        List<AssertionScope> scopes = query.getListDistinct();
        for (AssertionScope scope : scopes) {
            DatabaseManager.removeObject(scope);
        }
        OasisTestAssertionFactory.cleanOasisTestAssertions();
    }

    @Override
    protected String getDb() {
        return "assertion-manager-junit";
    }

    @Override
    protected String getHbm2ddl() {
        return "create-drop";
    }

    protected boolean getShowSql() {
        return true;
    }

    @Test
    @Ignore
    public void scopesHasAKeyword() {
        AssertionScope scope = new AssertionScope();
        scope.setKeyword("key");
        assertEquals("key", scope.getKeyword());
    }

    @Test
    @Ignore
    public void scopesHasADescription() {
        AssertionScope scope = new AssertionScope();
        scope.setDescription("description");
        assertEquals("description", scope.getDescription());
    }

    @Test
    @Ignore
    public void scopesIsLinkedToAssertions() {

        AssertionScope scope = new AssertionScope();
        scope.setDescription("description");
        scope.setKeyword("key");
        OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);

        List<OasisTestAssertion> assertions = new ArrayList<OasisTestAssertion>();
        assertions.add(assertion);
        scope.setAssertions(assertions);
        scope = (AssertionScope) DatabaseManager.writeObject(scope);
        DatabaseManager.update(assertion);

        assertEquals(1, scope.getAssertions().size());
        assertEquals(1, assertion.getScopes().size());
    }

    @Test
    @Ignore
    public void scopesRemoveAnAssertions() {

        AssertionScope scope = new AssertionScope();
        scope.setDescription("description");
        scope.setKeyword("key");
        OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);
        OasisTestAssertion assertion2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        assertion2 = (OasisTestAssertion) DatabaseManager.writeObject(assertion2);
        List<OasisTestAssertion> assertions = new ArrayList<OasisTestAssertion>();
        assertions.add(assertion);
        assertions.add(assertion2);

        scope.setAssertions(assertions);
        scope = (AssertionScope) DatabaseManager.writeObject(scope);
        DatabaseManager.update(assertion);
        DatabaseManager.update(assertion2);

        assertEquals(2, scope.getAssertions().size());
        assertEquals(1, assertion.getScopes().size());
        assertEquals(1, assertion2.getScopes().size());

        List<OasisTestAssertion> toDelete = new ArrayList<OasisTestAssertion>();
        toDelete.add(assertion2);
        scope.removeAssertion(toDelete);

        scope = (AssertionScope) DatabaseManager.writeObject(scope);
        DatabaseManager.update(assertion);
        DatabaseManager.update(assertion2);
        assertEquals(1, scope.getAssertions().size());
        assertEquals(1, assertion.getScopes().size());
        assertEquals(0, assertion2.getScopes().size());
    }

    @Test
    @Ignore
    public void scopesRemoveMultipleAssertions() {

        AssertionScope scope = new AssertionScope();
        scope.setDescription("description");
        scope.setKeyword("key");
        OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);
        OasisTestAssertion assertion2 = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        assertion2 = (OasisTestAssertion) DatabaseManager.writeObject(assertion2);
        List<OasisTestAssertion> assertions = new ArrayList<OasisTestAssertion>();
        assertions.add(assertion);
        assertions.add(assertion2);

        scope.setAssertions(assertions);
        scope = (AssertionScope) DatabaseManager.writeObject(scope);
        DatabaseManager.update(assertion);
        DatabaseManager.update(assertion2);

        assertEquals(2, scope.getAssertions().size());
        assertEquals(1, assertion.getScopes().size());
        assertEquals(1, assertion2.getScopes().size());

        List<OasisTestAssertion> toDelete = new ArrayList<OasisTestAssertion>();
        toDelete.add(assertion2);
        toDelete.add(assertion);

        scope.removeAssertion(toDelete);

        scope = (AssertionScope) DatabaseManager.writeObject(scope);
        DatabaseManager.update(assertion);
        DatabaseManager.update(assertion2);
        assertEquals(0, scope.getAssertions().size());
        assertEquals(0, assertion.getScopes().size());
        assertEquals(0, assertion2.getScopes().size());
    }

    @Test
    @Ignore
    public void scopelinkedToOtherScopes() {
        AssertionScope scope = createScopeWithAssertion("description", "key");
        AssertionScope scope2 = createScopeWithAssertion("description2", "key2");

        AssertionScope scope3 = createScopeWithAssertion("description3", "key3");

        List<AssertionScope> scopes = new ArrayList<AssertionScope>();
        scopes.add(scope2);
        scopes.add(scope3);
        scope.setLinkedScopes(scopes);
        scope = (AssertionScope) DatabaseManager.writeObject(scope);

        assertEquals(3, scope.getAllAssertions().size());
    }

    /*   _____________        _____________
     *   |   scope   |<-------| scope 4   |
     *   |           |        |           |
     *   -------------        -------------
     *         |       \             ^
     *         |        \            |
     *         |         \           |
     *         |          \          |
     *         |           \         |
     *         |            \        |
     *         |             \       |
     *   ______v______        v______|_____
     *   |   scope 2 |        | scope 3   |
     *   |           |        |           |
     *   -------------        -------------
     *
     * */
    @Test
    public void scopelinkedToComposedScopes() {
        AssertionScope scope = createScopeWithAssertion("description", "key");
        AssertionScope scope2 = createScopeWithAssertion("description2", "key2");
        AssertionScope scope3 = createScopeWithAssertion("description3", "key3");

        AssertionScope scope4 = createScopeWithAssertion("description4", "key4");

		/* link scopes 2 and 3 to scope*/
        List<AssertionScope> scopes = new ArrayList<AssertionScope>();
        scopes.add(scope2);
        scopes.add(scope3);
        scope.setLinkedScopes(scopes);
        scope = (AssertionScope) DatabaseManager.writeObject(scope);

		/*Link scope 4 to scope */
        List<AssertionScope> scopes_ = new ArrayList<AssertionScope>();
        scopes_.add(scope);
        scope4.setLinkedScopes(scopes_);
        scope4 = (AssertionScope) DatabaseManager.writeObject(scope4);

		/*scope 3 is already linked to scope4 through scope
         * It is to test if the algorithm checks recursive inclusions
		 * */
        List<AssertionScope> scopes__ = new ArrayList<AssertionScope>();
        scopes__.add(scope4);
        scope3.setLinkedScopes(scopes__);

        scope3 = (AssertionScope) DatabaseManager.writeObject(scope3);

        assertEquals(4, scope4.getAllAssertions().size());

        List<AssertionScope> assertionsScopesReferencing = AssertionScope.getAssertionsScopesReferencing(scope2, EntityManagerService.provideEntityManager());
        assertEquals(1, assertionsScopesReferencing.size());
        assertEquals(scope.getId(), assertionsScopesReferencing.get(0).getId());
    }

    private AssertionScope createScopeWithAssertion(String description, String key) {
        AssertionScope scope = new AssertionScope();
        scope.setDescription(description);
        scope.setKeyword(key);
        OasisTestAssertion assertion = OasisTestAssertionFactory.createTestAssertionWithMandatoryFields();
        assertion = (OasisTestAssertion) DatabaseManager.writeObject(assertion);

        List<OasisTestAssertion> assertions = new ArrayList<OasisTestAssertion>();
        assertions.add(assertion);
        scope.setAssertions(assertions);
        scope = (AssertionScope) DatabaseManager.writeObject(scope);
        return scope;
    }
}
