package net.ihe.gazelle.oasis.xmlloader;

/**
 * <b>Class Description : </b>Listener<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 25/07/16
 * @class Listener
 * @package net.ihe.gazelle.oasis.xmlloader
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
public interface Listener {

    void send(String message);
    void sendFinalMessage(String message);
}
