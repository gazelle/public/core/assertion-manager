package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@Entity
@Audited
@Name("oasisDerivedSourceItem")
@Table(name = "am_oasis_derived_source_item")
@SequenceGenerator(name = "am_oasis_derived_source_item_sequence", sequenceName = "am_oasis_derived_source_item_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisDerivedSourceItem extends GazellePersiste implements java.io.Serializable {

    private static final long serialVersionUID = -7874539336660763637L;
    public static final String TEXT = "text";

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_derived_source_item_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "name")
    @Lob
    @Type(type = TEXT)
    private String name;

    @Column(name = "uri")
    @Lob
    @Type(type = TEXT)
    private String uri;

    @Column(name = "content")
    @Lob
    @Type(type = TEXT)
    private String content;

    @Column(name = "document_id")
    @Lob
    @Type(type = TEXT)
    private String documentId;

    @Column(name = "version_id")
    @Lob
    @Type(type = TEXT)
    private String versionId;

    @Column(name = "revision_id")
    @Lob
    @Type(type = TEXT)
    private String revisionId;

    @Column(name = "resource_provenance_id")
    @Lob
    @Type(type = TEXT)
    private String resourceProvenanceId;

    @Column(name = "date_string")
    private String date;

    @ManyToMany(mappedBy = "derivedSourceItems")
    private transient List<OasisNormativeSource> normativeSources;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDocumentId() {
        return this.documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getVersionId() {
        return this.versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getRevisionId() {
        return this.revisionId;
    }

    public void setRevisionId(String revisionId) {
        this.revisionId = revisionId;
    }

    public String getResourceProvenanceId() {
        return this.resourceProvenanceId;
    }

    public void setResourceProvenanceId(String resourceProvenanceId) {
        this.resourceProvenanceId = resourceProvenanceId;
    }

    public List<OasisNormativeSource> getNormativeSources() {
        return this.normativeSources;
    }

    public void setNormativeSources(List<OasisNormativeSource> normativeSources) {
        this.normativeSources = normativeSources;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
