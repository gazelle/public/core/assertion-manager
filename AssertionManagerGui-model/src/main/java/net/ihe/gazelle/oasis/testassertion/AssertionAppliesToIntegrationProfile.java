package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToIntegrationProfileQuery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
@DiscriminatorValue(value = AssertionAppliesToIntegrationProfile.DB_DISCRIMINATOR)
public class AssertionAppliesToIntegrationProfile extends AbstractAssertionAppliesTo implements Serializable {

    public static final String DB_DISCRIMINATOR = "TF_INTEGRATION_PROFILE";
    private static final long serialVersionUID = -3850463454020298687L;

    public AssertionAppliesToIntegrationProfile(String entityKeyword, String entityName, int entityId, String entityProvider, String permalink, OasisTestAssertion assertion) {
        super(entityKeyword, entityName, entityId, entityProvider, permalink, assertion);
    }

    public AssertionAppliesToIntegrationProfile() {
        super();
    }

    public List<AssertionAppliesToIntegrationProfile> getAssertionsLinked() {
        AssertionAppliesToIntegrationProfileQuery query = new AssertionAppliesToIntegrationProfileQuery();
        query.linkedEntityKeyword().eq(this.getLinkedEntityKeyword());
        query.id().neq(this.getId());
        return query.getList();
    }
}
