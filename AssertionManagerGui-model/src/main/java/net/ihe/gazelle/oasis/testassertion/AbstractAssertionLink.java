package net.ihe.gazelle.oasis.testassertion;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractAssertionLink extends AssertionLink {

    public AbstractAssertionLink(String entityKeyword, OasisTestAssertion assertion, String linkedEntityProvider,
                                 String linkedEntityPermalink, String linkedEntityName) {
        super();
        super.setLinkedEntityKeyword(entityKeyword);
        this.assertion = assertion;
        super.setLinkedEntityPermalink(linkedEntityPermalink);
        super.setLinkedEntityProvider(linkedEntityProvider);
        setLinkedEntityName(linkedEntityName);
    }

    public AbstractAssertionLink() {
        super();
    }

    /**
     * Field assertion.
     */
    @ManyToOne
    @JoinColumn(name = "assertion_id", nullable = false)
    private OasisTestAssertion assertion;

    /**
     * Method getAssertion.
     *
     * @return OasisTestAssertion
     */
    public OasisTestAssertion getAssertion() {
        return assertion;
    }

    /**
     * Method setAssertion.
     *
     * @param assertion OasisTestAssertion
     */
    public void setAssertion(OasisTestAssertion assertion) {
        this.assertion = assertion;
    }

}
