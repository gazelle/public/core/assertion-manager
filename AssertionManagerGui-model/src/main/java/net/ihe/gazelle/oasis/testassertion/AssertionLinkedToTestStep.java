package net.ihe.gazelle.oasis.testassertion;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = AssertionLinkedToTestStep.DB_DISCRIMINATOR)
public class AssertionLinkedToTestStep extends AbstractAssertionLink implements Serializable {

    public static final String DB_DISCRIMINATOR = "TEST_STEP";
    private static final long serialVersionUID = 8346565970594776057L;

    public AssertionLinkedToTestStep(String entityKeyword, OasisTestAssertion assertion, String linkedEntityProvider,
                                     String linkedEntityPermalink, String linkedEntityName) {
        super(entityKeyword, assertion, linkedEntityProvider, linkedEntityPermalink, linkedEntityName);
    }

    public AssertionLinkedToTestStep() {
        super();
    }

    public int getTestStepId() {
        return Integer.valueOf(getLinkedEntityKeyword()).intValue();
    }
}
