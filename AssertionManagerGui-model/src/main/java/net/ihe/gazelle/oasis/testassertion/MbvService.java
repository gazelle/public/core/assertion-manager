package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.common.model.AuditedObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Audited
@Name("mbvService")
@Table(name = "am_mbv_service")
@SequenceGenerator(name = "am_mbv_service_sequence", sequenceName = "am_mbv_service_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
public class MbvService extends AuditedObject implements java.io.Serializable {


    private static final long serialVersionUID = 4896085691144167725L;
    public static final int MAX_LENGTH = 64;

    public MbvService() {
        super();
        this.updateStatus = UpdateStatusEnum.NeverSynchronized;
    }


    public MbvService(String keyword, String url) {
        super();
        this.keyword = keyword;
        this.url = url;
        this.updateStatus = UpdateStatusEnum.NeverSynchronized;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_mbv_service_sequence")
    private Integer id;

    @Column(name = "name", unique = true, nullable = false)
    @Size(max = MAX_LENGTH)
    private String keyword;

    @Column(name = "url", unique = true, nullable = false)
    private String url;

    @Column(name = "update_status")
    private UpdateStatusEnum updateStatus;

    @OneToMany(mappedBy = "mbvService", cascade = CascadeType.ALL)
    private List<AssertionLinkedToMBV> assertions;

    @OneToMany(mappedBy = "mbvService")
    private List<MbvCoverageStatus> coverageStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated")
    private Date lastUpdated;

    public Date getLastUpdated() {
        return new Date(lastUpdated.getTime());
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = new Date(lastUpdated.getTime());
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AssertionLinkedToMBV> getAssertions() {
        return assertions;
    }

    public void setAssertions(List<AssertionLinkedToMBV> assertions) {
        this.assertions = assertions;
    }

    public UpdateStatusEnum getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(UpdateStatusEnum updateStatus) {
        this.updateStatus = updateStatus;
    }

    public List<MbvCoverageStatus> getCoverageStatus() {
        return coverageStatus;
    }

    public void setCoverageStatus(List<MbvCoverageStatus> coverageStatus) {
        this.coverageStatus = coverageStatus;
    }

    public enum UpdateStatusEnum {
        NeverSynchronized("Never synchronized"),
        InProgress("In progress"),
        Updated("Updated");

        private String friendlyName;

        UpdateStatusEnum(String friendlyName) {
            this.friendlyName = friendlyName;
        }

        public String getFriendlyName() {
            return friendlyName;
        }
    }

    public String getUpdateProgress() {
        String progress = "";
        String cachedProgress;
        cachedProgress = CacheManager.get(getUrl());
        if (cachedProgress != null) {
            progress = " (" + cachedProgress + ")";
        }
        return progress;
    }

    public void setUpdateProgress(String progress) {
        CacheManager.put(getUrl(), progress);
    }

    public List<MbvCoverageStatus> getAssertionsCovered(EntityManager em) {
        MbvCoverageStatusQuery query = new MbvCoverageStatusQuery(em);
        query.mbvService().id().eq(id);
        query.coverageStatus().eq(MbvCoverageStatus.CoverageStatusEnum.Covered);

        return query.getListDistinct();
    }

    public List<String> getMissingOasisTarget(EntityManager em) {
        List<String> missingOasisTarget = new ArrayList<String>();
        MbvCoverageStatusQuery query = new MbvCoverageStatusQuery(em);
        query.mbvService().id().eq(id);
        query.coverageStatus().eq(MbvCoverageStatus.CoverageStatusEnum.Missing);
        for(MbvCoverageStatus currentMbvCoverageStatus : query.getListDistinct()){
            OasisTargetQuery queryOasisTarget = new OasisTargetQuery();
            String idScheme = currentMbvCoverageStatus.getIdScheme();
            queryOasisTarget.idScheme().eq(idScheme);
            OasisTarget oasisTarget = queryOasisTarget.getUniqueResult();
            if(oasisTarget == null && !missingOasisTarget.contains(idScheme)){
                missingOasisTarget.add(idScheme);
            }
        }
        return missingOasisTarget;
    }

    public List<MbvCoverageStatus> getMissingOasisTestAssertion(EntityManager em) {
        MbvCoverageStatusQuery query = new MbvCoverageStatusQuery(em);
        query.mbvService().id().eq(id);
        query.coverageStatus().eq(MbvCoverageStatus.CoverageStatusEnum.Missing);

        return query.getListDistinct();
    }

    public List<MbvCoverageStatus> getCoverageRemoved(EntityManager em) {
        MbvCoverageStatusQuery query = new MbvCoverageStatusQuery(em);
        query.mbvService().id().eq(id);
        query.coverageStatus().eq(MbvCoverageStatus.CoverageStatusEnum.Removed);

        return query.getListDistinct();
    }

    public int assertionCount() {
        MbvServiceQuery q = new MbvServiceQuery();
        q.id().eq(this.id);
        return q.assertions().id().getCountOnPath();
    }

    public boolean hasNeverBeenUpdated(){
        if(this.lastUpdated == null){
            return true;
        }
        return false;
    }
}
