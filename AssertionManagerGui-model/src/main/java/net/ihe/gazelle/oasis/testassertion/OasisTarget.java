package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Audited
@Name("oasisTarget")
@Table(name = "am_oasis_target")
@SequenceGenerator(name = "am_oasis_target_sequence", sequenceName = "am_oasis_target_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisTarget extends GazellePersiste implements java.io.Serializable, Comparable<OasisTarget> {


    private static final long serialVersionUID = -1963022492112524370L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_target_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "content")
    @Lob
    @Type(type = "text")
    @XmlAttribute(name = "descriptionsScheme")
    private String content;

    @Column(name = "type")
    @Lob
    @Type(type = "text")
    private String type;

    @Column(name = "idScheme", nullable = false, unique = true)
    @XmlAttribute(name = "idscheme")
    private String idScheme;

    @Column(name = "language")
    private OasisLanguageValues language;

    @OneToMany(mappedBy = "target", cascade = {CascadeType.REMOVE})
    @XmlTransient
    private List<OasisTestAssertion> testAssertions;

    public OasisTarget() {

    }

    public OasisTarget(String idScheme) {
        this.idScheme = idScheme;
    }

    public List<OasisTestAssertion> getTestAssertions() {
        return testAssertions;
    }

    public void setTestAssertions(List<OasisTestAssertion> testAssertions) {
        this.testAssertions = testAssertions;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdScheme() {
        return this.idScheme;
    }

    public void setIdScheme(String idScheme) {
        this.idScheme = idScheme;
    }

    public OasisLanguageValues getLanguage() {
        return this.language;
    }

    public void setLanguage(OasisLanguageValues language) {
        this.language = language;
    }

    public static boolean exists(String idScheme, EntityManager entityManager) {
        OasisTargetQuery query = new OasisTargetQuery(entityManager);
        query.idScheme().eq(idScheme);
        return query.getUniqueResult() != null;
    }

    public static List<String> getIdSchemes(EntityManager entityManager) {
        OasisTargetQuery query = new OasisTargetQuery(entityManager);
        return query.idScheme().getListDistinct();
    }

    public static List<OasisTarget> getIdSchemesObjects(EntityManager entityManager) {
        OasisTargetQuery query = new OasisTargetQuery(entityManager);
        return query.getListDistinct();
    }

    public List<OasisRefSourceItem> getDocuments() {
        return getDocuments(EntityManagerService.provideEntityManager());
    }

    public List<OasisRefSourceItem> getDocuments(EntityManager entityManager) {

        OasisTargetQuery query = new OasisTargetQuery(entityManager);
        query.id().eq(this.id);
        List<OasisNormativeSource> normativeSources = query.testAssertions().normativeSource().getListDistinct();
        List<OasisRefSourceItem> refSourceItems = new ArrayList<OasisRefSourceItem>();
        for (OasisNormativeSource oasisNormativeSource : normativeSources) {
            refSourceItems.addAll(oasisNormativeSource.getRefSourceItems());
        }
        return refSourceItems;
    }

    @Override
    public int compareTo(OasisTarget o) {
        if (o == null || o.idScheme == null) {
            return -1;
        }
        if (this.idScheme == null) {
            return 1;
        }
        return this.idScheme.compareToIgnoreCase(o.idScheme);
    }

    public void linkToDocument(EntityManager em, List<OasisRefSourceItem> sources) {
        for (OasisTestAssertion testAssertion : getTestAssertions()) {
            testAssertion.linkToDocument(em, sources);
        }
        em.flush();
    }

    public void updateCoverageCounts(EntityManager entityManager) {
        OasisTestAssertion.updateCoverageCounts(entityManager, this.getTestAssertions());
    }
}
