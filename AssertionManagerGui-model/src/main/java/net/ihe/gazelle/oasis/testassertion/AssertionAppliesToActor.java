package net.ihe.gazelle.oasis.testassertion;

import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToActorQuery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
@DiscriminatorValue(value = AssertionAppliesToActor.DB_DISCRIMINATOR)
public class AssertionAppliesToActor extends AbstractAssertionAppliesTo implements Serializable {

    public static final String DB_DISCRIMINATOR = "TF_ACTOR";
    private static final long serialVersionUID = -3627814743891943113L;

    public AssertionAppliesToActor(String entityKeyword, String entityName, int entityId, String entityProvider, String permalink, OasisTestAssertion assertion) {
        super(entityKeyword, entityName, entityId, entityProvider, permalink, assertion);
    }

    public AssertionAppliesToActor() {
        super();
    }

    public List<AssertionAppliesToActor> getAssertionsLinked() {
        AssertionAppliesToActorQuery query = new AssertionAppliesToActorQuery();
        query.linkedEntityKeyword().eq(this.getLinkedEntityKeyword());
        query.id().neq(this.getId());
        return query.getList();
    }

}
