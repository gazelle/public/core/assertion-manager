package net.ihe.gazelle.oasis.testassertion;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@Entity
@Audited
@Name("oasisComment")
@Table(name = "am_oasis_comment")
@SequenceGenerator(name = "am_oasis_comment_sequence", sequenceName = "am_oasis_comment_id_seq", allocationSize = 1)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "instances")
@XmlAccessorType(XmlAccessType.FIELD)
public class OasisComment extends GazellePersiste implements java.io.Serializable {

    private static final long serialVersionUID = 5772333055594882565L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "am_oasis_comment_sequence")
    @XmlTransient
    private Integer id;

    @Column(name = "content")
    @Lob
    @Type(type = "text")
    private String content;

    @OneToMany(mappedBy = "comment")
    private List<OasisNormativeSource> normativeSources;

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<OasisNormativeSource> getNormativeSources() {
        return normativeSources;
    }

    public void setNormativeSources(List<OasisNormativeSource> normativeSources) {
        this.normativeSources = normativeSources;
    }
}
