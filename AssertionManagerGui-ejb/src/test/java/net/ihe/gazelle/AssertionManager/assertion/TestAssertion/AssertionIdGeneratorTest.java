package net.ihe.gazelle.AssertionManager.assertion.TestAssertion;

import net.ihe.gazelle.AssertionManager.assertion.AssertionIdGenerator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AssertionIdGeneratorTest {

    @Test
    public void generateNextAssertionId() throws Exception {
        assertEquals("Conf-02", AssertionIdGenerator.getNextAssertionId("Conf-01"));
    }

    @Test
    public void generateNextAssertionIdWithEmptyString() throws Exception {
        assertEquals("Conf-001", AssertionIdGenerator.getNextAssertionId("Conf"));
    }

    @Test
    public void generateNextAssertionId1() throws Exception {
        assertEquals("Conf-0002", AssertionIdGenerator.getNextAssertionId("Conf-0001"));
    }
}
