package net.ihe.gazelle.AssertionManager.document;

import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.junit.HibernateConfiguration;

import java.net.UnknownHostException;

/**
 * <b>Class Description : </b>AssertionManagerTestQueryJunit4<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 * @class AssertionManagerTestQueryJunit4
 * @package net.ihe.gazelle.AssertionManager.document
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
public class AssertionManagerTestQueryJunit4 extends AbstractTestQueryJunit4 {
    protected String getDb() {
        return "assertion-manager-junit";
    }

}
