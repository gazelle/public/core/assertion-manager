package net.ihe.gazelle.AssertionManager.scope;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionScope;
import net.ihe.gazelle.oasis.testassertion.AssertionScopeQuery;

import javax.persistence.EntityManager;
import java.util.Map;

public class AssertionScopeFilter extends Filter<AssertionScope> implements QueryModifier<AssertionScope> {


    private static final long serialVersionUID = -1599167299359890043L;
    private AssertionScope excludeScopeAndLinkedScopes;

    public AssertionScopeFilter(Map<String, String> requestParameterMap) {
        super(getHQLCriterions(), requestParameterMap);
        queryModifiers.add(this);
    }

    private static HQLCriterionsForFilter<AssertionScope> getHQLCriterions() {

        AssertionScopeQuery query = new AssertionScopeQuery(EntityManagerService.provideEntityManager());
        HQLCriterionsForFilter<AssertionScope> result = query.getHQLCriterionsForFilter();

        result.addPath("keyword", query.keyword());

        return result;
    }

    @Override
    public EntityManager getEntityManager() {
        return EntityManagerService.provideEntityManager();
    }

    public void setExcludeScopeAndLinkedScopes(AssertionScope scope) {
        this.excludeScopeAndLinkedScopes = scope;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<AssertionScope> queryBuilder, Map<String, Object> filterValuesApplied) {
        if (excludeScopeAndLinkedScopes != null) {
            AssertionScopeQuery query = new AssertionScopeQuery(queryBuilder);
            query.neq(excludeScopeAndLinkedScopes);
            query.nin(excludeScopeAndLinkedScopes.getLinkedScopes());
        }
    }
}
