package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.AssertionManager.utils.UrlHelper;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("idSchemesManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "IdSchemesManagerLocal")
public class IdSchemesManager implements IdSchemesManagerLocal, Serializable {

    public IdSchemesManager() {
        super();
        createNewIdScheme = false;
    }


    private static final long serialVersionUID = 5461834887444191276L;

    private static final Logger LOG = LoggerFactory.getLogger(IdSchemesManager.class);

    private OasisTarget selectedOasisTarget;
    private IdSchemesCoverage selectedIdSchemeCoverage;

    private IdSchemesFilter filter;
    private FilterDataModel<OasisTarget> targets;
    private String selectedScopeForFilter = "";

    private boolean createNewIdScheme;

    public List<IdSchemesCoverage> getIdSchemes() {
        OasisTargetQuery query = new OasisTargetQuery(EntityManagerService.provideEntityManager());

        if (getSelectedScopeForFilter() != null && !"".equals(getSelectedScopeForFilter())) {
            AssertionScopeQuery assertionQuery = new AssertionScopeQuery(EntityManagerService.provideEntityManager());

            assertionQuery.keyword().eq(selectedScopeForFilter);

            AssertionScope selectedScope = assertionQuery.getUniqueResult();

            List<Integer> scopesids = new ArrayList<Integer>();
            AssertionScope.getLinkedScopeIds(selectedScope, scopesids);

            query.testAssertions().scopes().id().in(scopesids);
        }

        if (!"".equals(getFilteredIdScheme())) {
            query.idScheme().eq(getFilteredIdScheme());
        }

        if (!"".equals(getFilteredMbvService())) {
            query.testAssertions().mbv().mbvService().keyword().eq(getFilteredMbvService());
        }
        List<OasisTarget> idSchemes = query.getListDistinct();
        return IdSchemesCoverage.compute(idSchemes, getSelectedScopeForFilter());
    }

    public String delete() {
        OasisTargetQuery query = new OasisTargetQuery(new HQLQueryBuilder<OasisTarget>(
                EntityManagerService.provideEntityManager(), OasisTarget.class));
        query.id().eq(selectedOasisTarget.getId());
        OasisTarget targetToDelete = query.getUniqueResult();

        LOG.info("delete Target" + targetToDelete.getIdScheme());
        try {
            EntityManagerService.provideEntityManager().remove(targetToDelete);
            FacesMessages.instance().add(targetToDelete.getIdScheme() + " successfully deleted");
        } catch (Exception e) {
            FacesMessages.instance().add("Cannot delete " + targetToDelete.getIdScheme());
            LOG.warn("Cannot delete " + targetToDelete.getIdScheme());
        }
        return "/idSchemes/index.seam";
    }

    public void setSelectedIdScheme(IdSchemesCoverage idScheme) {
        String idScheme2 = idScheme.getIdScheme();
        OasisTargetQuery query = new OasisTargetQuery(EntityManagerService.provideEntityManager());
        query.idScheme().eq(idScheme2);

        this.selectedOasisTarget = query.getUniqueResult();
    }

    public OasisTarget getSelectedIdScheme() {
        return selectedOasisTarget;
    }

    @SuppressWarnings("unchecked")
    public IdSchemesFilter getFilter() {
        if (filter == null) {
            final FacesContext fc = FacesContext.getCurrentInstance();
            final Map<String, String> requestParameterMap = fc.getExternalContext().getRequestParameterMap();
            filter = new IdSchemesFilter(requestParameterMap);

            //fixing encoding
            try {
                UrlHelper.fixEncoding(filter);
            } catch (Exception e) {
                LOG.error("Encoding error {} : ", e.getMessage());
            }
        }
        return filter;
    }

    public FilterDataModel<OasisTarget> getScopeAssertions() {
        if (targets == null) {
            targets = new FilterDataModel<OasisTarget>(getFilter()) {
                @Override
                protected Object getId(OasisTarget t) {
                    return t.getId();
                }
            };
        }
        return targets;
    }

    private String getFilteredMbvService() {
        return getSelectedFilterValue("mbvService");
    }

    private String getFilteredIdScheme() {
        return getSelectedFilterValue("idScheme");
    }

    private String getSelectedFilterValue(String filterField) {
        String result = "";
        if (!getFilter().getFilterValues().isEmpty()) {
            Object object = getFilter().getFilterValues().get(filterField);
            if (object != null) {
                result = object.toString();
            }
        }
        return result;
    }

    public IdSchemesCoverage getSelectedIdSchemeCoverage() {
        selectedIdSchemeCoverage = new IdSchemesCoverage(getSelectedOasisTarget().getIdScheme());
        selectedIdSchemeCoverage.compute();
        return selectedIdSchemeCoverage;
    }

    public void setSelectedIdSchemeCoverage(IdSchemesCoverage selectedIdSchemeCoverage) {
        this.selectedIdSchemeCoverage = selectedIdSchemeCoverage;
    }

    public OasisTarget getSelectedOasisTarget() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String idSchemeId = fc.getExternalContext().getRequestParameterMap().get("idSchemeId");
        String newOasisTarget = fc.getExternalContext().getRequestParameterMap().get("new");
        if (idSchemeId != null) {
            EntityManager em = EntityManagerService.provideEntityManager();
            setSelectedOasisTarget(em.find(OasisTarget.class, Integer.valueOf(idSchemeId)));
        } else if ("1".equals(newOasisTarget)) {
            String idScheme = fc.getExternalContext().getRequestParameterMap().get("idScheme");
            setSelectedOasisTarget(new OasisTarget(idScheme));
        }
        return selectedOasisTarget;
    }

    private void setSelectedOasisTarget(OasisTarget target) {
        selectedOasisTarget = target;
    }

    public List<OasisRefSourceItem> getDocuments() {
        return getSelectedOasisTarget().getDocuments(EntityManagerService.provideEntityManager());
    }

    public String getSelectedScopeForFilter() {
        return selectedScopeForFilter;
    }

    public void setSelectedScopeForFilter(String selectedScopeForFilterIn) {
        this.selectedScopeForFilter = selectedScopeForFilterIn;
    }

    public void resetSelectedScopeForFilter() {
        this.selectedScopeForFilter = "";
    }

    public String saveNewTarget() {
        LOG.info("Save selectedOasisTarget: " + selectedOasisTarget.getIdScheme());
        EntityManager em = EntityManagerService.provideEntityManager();
        selectedOasisTarget = em.merge(selectedOasisTarget);

        FacesMessages.instance().add(selectedOasisTarget.getIdScheme() + " idScheme was successfully created");
        return "/idSchemes/index.seam";
    }

    public boolean createNewIdScheme() {
        return createNewIdScheme;
    }

    public void newIdScheme() {
        createNewIdScheme = true;
        setSelectedOasisTarget(new OasisTarget());
    }

    public void clearFilter() {
        resetSelectedScopeForFilter();
        getFilter().clear();
    }

    public boolean validateIdScheme() {
        if (selectedOasisTarget == null) {
            LOG.info("IdScheme is null");
            return false;
        }
        if ((selectedOasisTarget.getIdScheme() == null) || selectedOasisTarget.getIdScheme().trim().equals("")) {
            LOG.info("IdScheme is null or empty");
            return false;
        }

        OasisTargetQuery queryOriginalIdScheme = new OasisTargetQuery();
        queryOriginalIdScheme.id().eq(selectedOasisTarget.getId());
        OasisTarget originalIdScheme = queryOriginalIdScheme.getUniqueResult();

        if(originalIdScheme == null || (originalIdScheme != null && !selectedOasisTarget.getIdScheme().equals(originalIdScheme.getIdScheme()))){
            OasisTargetQuery query = new OasisTargetQuery();
            query.idScheme().eq(selectedOasisTarget.getIdScheme());
            List<OasisTarget> listDistinct = query.getListDistinct();
            if (listDistinct.size() > 0) {
                FacesMessages.instance().addToControl("IdScheme", "Already used");
                LOG.info("IdScheme is already used");
                return false;
            }
            LOG.info("IdScheme is ok");
        }

        return true;
    }
}
