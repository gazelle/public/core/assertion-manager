package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.oasis.testassertion.AssertionScope;
import net.ihe.gazelle.oasis.testassertion.AssertionScopeQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertionQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>OasisTestAssertionCoverageQueryHelper<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 29/09/16
 * @class OasisTestAssertionCoverageQueryHelper
 * @package net.ihe.gazelle.AssertionManager.idScheme
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
public class OasisTestAssertionCoverageQueryHelper extends OasisTestAssertionQuery {
    private static final long serialVersionUID = -656086103652220975L;

    OasisTestAssertionCoverageQueryHelper(String idScheme, String scope) {
        filterScope(scope);
        this.target().idScheme().eq(idScheme);
    }

    public OasisTestAssertionCoverageQueryHelper(String scope) {
        filterScope(scope);
    }


    private void filterScope(String scope) {
        if (scope != null && !"".equals(scope)) {

            AssertionScopeQuery assertionQuery = new AssertionScopeQuery();

            assertionQuery.keyword().eq(scope);

            AssertionScope selectedScope = assertionQuery.getUniqueResult();

            List<Integer> scopesIds = new ArrayList<Integer>();
            AssertionScope.getLinkedScopeIds(selectedScope, scopesIds);

            scopes().id().in(scopesIds);
        }
    }


    public int countCoveredAssertions() {
        coverableAssertions();
        coveredByTestOrTestStepOrRuleOrMbv();
        return this.getCount();
    }

    private void coverableAssertions() {
        addRestriction(onlyTestableAssertion());
    }

    private HQLRestriction onlyTestableAssertion() {
        return this.testable().eqRestriction(true);
    }


    private void coveredByTestOrTestStepOrRuleOrMbv() {
        this.getQueryBuilder().forceOuterJoins();

        this.addRestriction(HQLRestrictions.or(
                this.tests().deprecated().eqRestriction(false),
                this.testSteps().deprecated().eqRestriction(false),
                this.tfRules().deprecated().eqRestriction(false),
                this.mbv().isNotEmptyRestriction()
        ));
    }

    public int countNotTestableAssertions() {
        this.testable().eq(false);
        return this.getCount();
    }

    public int countAssertionCoveredThroughTests() {
        coverableAssertions();
        coveredByTest();
        return this.getCount();
    }

    private void coveredByTest() {
        this.getQueryBuilder().forceOuterJoins();
        addRestriction(HQLRestrictions.or(
                this.tests().deprecated().eqRestriction(false),
                this.testSteps().deprecated().eqRestriction(false)
        ));
    }

    public int countAssertionCoveredThroughRules() {
        coverableAssertions();
        this.tfRules().isNotEmpty();
        return this.getCount();
    }

    public int countAssertionCoveredThroughMbv() {
        coverableAssertions();
        this.mbv().isNotEmpty();
        return this.getCount();
    }
}
