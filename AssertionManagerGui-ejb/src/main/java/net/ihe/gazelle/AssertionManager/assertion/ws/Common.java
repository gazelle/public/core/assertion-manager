package net.ihe.gazelle.AssertionManager.assertion.ws;

import net.ihe.gazelle.oasis.testassertion.OasisNormativeSource;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "common")
public class Common implements Serializable {

    private static final long serialVersionUID = 8047488707240936013L;
    private OasisNormativeSource normativeSource;
    private OasisTarget target;

    public OasisTarget getTarget() {
        return target;
    }

    public void setTarget(OasisTarget targetIn) {
        this.target = targetIn;
    }

    public OasisNormativeSource getNormativeSource() {
        return normativeSource;
    }

    public void setNormativeSource(OasisNormativeSource normativeSourceIn) {
        this.normativeSource = normativeSourceIn;
    }
}
