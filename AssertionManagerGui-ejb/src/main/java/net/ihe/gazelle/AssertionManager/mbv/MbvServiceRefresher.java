package net.ihe.gazelle.AssertionManager.mbv;

import net.ihe.gazelle.AssertionManager.assertion.AssertionManager;
import net.ihe.gazelle.assertion.coverage.provider.client.CoverageProviderClient;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertion;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertionWrapper;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.dates.TimestampNano;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.*;
import net.ihe.gazelle.oasis.testassertion.MbvCoverageStatus.CoverageStatusEnum;
import net.ihe.gazelle.oasis.testassertion.MbvService.UpdateStatusEnum;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.async.QuartzDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("MbvServiceRefresher")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("MbvServiceRefresherLocal")
public class MbvServiceRefresher implements MbvServiceRefresherLocal, Serializable {


    private static final long serialVersionUID = 6556970342523420826L;

    private static final Logger LOG = LoggerFactory.getLogger(MbvServiceRefresher.class);

    public void setMbvService(MbvService mbvService) {
        this.mbvService = mbvService;
    }

    private MbvService mbvService;
    private List<Integer> mbvServiceIds;


    @Override
    @Observer("updateMbvCoverage")
    @Transactional
    public void updateMbvCoverage(List<Integer> mbvServiceIdIn) {

        if (mbvServiceIdIn.size() > 0) {
            EntityManager em = EntityManagerService.provideEntityManager();
            Integer mbvServiceId = getMbvServiceId(mbvServiceIdIn);
            LOG.info("updateMbvCoverage: " + mbvServiceId);
            this.mbvService = em.find(MbvService.class, mbvServiceId);
            refreshCoverage();
        }
    }


    public void refreshCoverage() {
        mbvService.setUpdateStatus(UpdateStatusEnum.InProgress);
        mbvService.setUpdateProgress("preparing");
        EntityManager em = EntityManagerService.provideEntityManager();
        em.merge(mbvService);
        em.flush();
        List<WsAssertion> assertions = getAssertionsFromServer();
        updateAssertions(assertions);
    }

    protected void updateAssertions(List<WsAssertion> updatedListAssertionsCoveredByTheMbvService) {
        LOG.info("removePreviousStatus");
        int total = updatedListAssertionsCoveredByTheMbvService.size();

        removePreviousStatus();
        List<WsAssertion> noMoreCoveredAssertions =
                getNoMoreCoveredAssertions(updatedListAssertionsCoveredByTheMbvService);
        setNoMoreCoveredAssertionStatus(noMoreCoveredAssertions);


        EntityManager em = EntityManagerService.provideEntityManager();
        Query query = em
                .createQuery("delete FROM AssertionLinkedToMBV where mbvService = :mbvService");
        query.setParameter("mbvService", mbvService);
        long assertionLinkedToMBVRemoved = query.executeUpdate();
        em.flush();
        LOG.info("Removed " + assertionLinkedToMBVRemoved);

        mbvService.setUpdateProgress("0/" + total);
        LOG.info("Start update");
        QuartzDispatcher.instance().scheduleAsynchronousEvent("linkMbvServiceToAssertionJob", updatedListAssertionsCoveredByTheMbvService, 1, total);
    }

    private void setNoMoreCoveredAssertionStatus(List<WsAssertion> noMoreCoveredAssertions) {
        EntityManager em = EntityManagerService.provideEntityManager();
        for (WsAssertion noMoreCoveredAssertion : noMoreCoveredAssertions) {
            em.merge(new MbvCoverageStatus(CoverageStatusEnum.Removed, noMoreCoveredAssertion.getIdScheme(), noMoreCoveredAssertion.getAssertionId(), mbvService));
        }
    }

    private List<WsAssertion> getNoMoreCoveredAssertions(List<WsAssertion> updatedListAssertionsCoveredByTheMbvService) {
        List<WsAssertion> noMoreCoveredAssertion = new ArrayList<>();

        for (WsAssertion assertion : getAlreadyLinkedAssertions()) {
            if (!updatedListAssertionsCoveredByTheMbvService.contains(assertion)) {
                noMoreCoveredAssertion.add(assertion);
            }
        }
        return noMoreCoveredAssertion;
    }

    @Observer("linkMbvServiceToAssertionJob")
    @Transactional
    @Asynchronous
    public void linkMbvServiceToAssertionJob(List<WsAssertion> updatedListAssertionsCoveredByTheMbvService, int counter, int total) {
        if (updatedListAssertionsCoveredByTheMbvService.size() > 0) {
            WsAssertion wsAssertion = updatedListAssertionsCoveredByTheMbvService.get(0);
            updatedListAssertionsCoveredByTheMbvService.remove(0);
            linkMbvServiceToAssertion(wsAssertion);
            mbvService.setUpdateProgress(counter + "/" + total);
            QuartzDispatcher.instance().scheduleAsynchronousEvent("linkMbvServiceToAssertionJob", updatedListAssertionsCoveredByTheMbvService, next(counter), total);
        } else {
            LOG.info("Synchro finished");

            EntityManager em = EntityManagerService.provideEntityManager();
            MbvService mbvServiceTmp = em.find(MbvService.class, mbvService.getId());
            mbvServiceTmp.setLastUpdated(new TimestampNano());
            mbvServiceTmp.setUpdateStatus(UpdateStatusEnum.Updated);
            em.merge(mbvServiceTmp);
            em.flush();
            AssertionManager.updateCoverageCounts(em);

            QuartzDispatcher.instance().scheduleAsynchronousEvent("updateMbvCoverage", mbvServiceIds);
        }
    }

    private int next(int counter) {
        return counter + 1;
    }

    public void removePreviousStatus() {
        EntityManager em = EntityManagerService.provideEntityManager();
        mbvService = em.find(MbvService.class, mbvService.getId());
        Query query = em
                .createQuery("delete FROM MbvCoverageStatus where mbvService = :mbvService");
        query.setParameter("mbvService", mbvService);
        query.executeUpdate();
        em.flush();
        mbvService = em.find(MbvService.class, mbvService.getId());
    }

    private List<WsAssertion> getAlreadyLinkedAssertions() {
        List<WsAssertion> assertionsAlreadyLinkedToTheMbvService = new ArrayList<WsAssertion>();

        if (mbvService.getAssertions() != null) {
            for (AssertionLinkedToMBV assertionAlreadyLinkedToTheMbvServiceTmp : mbvService.getAssertions()) {
                assertionsAlreadyLinkedToTheMbvService.add(new WsAssertion(assertionAlreadyLinkedToTheMbvServiceTmp.getAssertion().getTarget().getIdScheme(), assertionAlreadyLinkedToTheMbvServiceTmp
                        .getAssertion().getAssertionId(), null));
            }
        }
        return assertionsAlreadyLinkedToTheMbvService;
    }

    private boolean oasisTestAssertionExists(OasisTestAssertion oasisAssertion) {
        return oasisAssertion != null;
    }

    private void linkMbvServiceToAssertion(WsAssertion wsAssertion) {
        EntityManager em = EntityManagerService.provideEntityManager();
        OasisTestAssertion oasisAssertion = findOasisTestAssertion(wsAssertion);

        if (oasisTestAssertionExists(oasisAssertion)) {
            if (!isTheServiceAlreadyLinkedToThisAssertion(oasisAssertion)) {
                em.merge(new AssertionLinkedToMBV(oasisAssertion, mbvService));
            }

            em.merge(new MbvCoverageStatus(CoverageStatusEnum.Covered, wsAssertion.getIdScheme(), wsAssertion.getAssertionId(), mbvService));
        } else {
            em.merge(new MbvCoverageStatus(CoverageStatusEnum.Missing, wsAssertion.getIdScheme(), wsAssertion.getAssertionId(), mbvService));
        }
        em.flush();
    }

    private OasisTestAssertion findOasisTestAssertion(WsAssertion wsAssertion) {
        EntityManager em = EntityManagerService.provideEntityManager();
        OasisTestAssertionQuery query = new OasisTestAssertionQuery(em);
        query.assertionId().eq(wsAssertion.getAssertionId());
        query.target().idScheme().eq(wsAssertion.getIdScheme());

        return query.getUniqueResult();
    }

    private List<WsAssertion> getAssertionsFromServer() {
        CoverageProviderClient client = new CoverageProviderClient(mbvService.getUrl());
        WsAssertionWrapper testAssertionsCovered = client.getTestAssertionsCovered();
        LOG.info("refreshCoverage: " + mbvService.getUrl() + " covers: " + testAssertionsCovered.getAssertions().size());
        return testAssertionsCovered.getAssertions();
    }

    private boolean isTheServiceAlreadyLinkedToThisAssertion(OasisTestAssertion oasisAssertion) {
        EntityManager em = EntityManagerService.provideEntityManager();
        AssertionLinkedToMBVQuery assertionLinkMbvQuery = new AssertionLinkedToMBVQuery(em);
        assertionLinkMbvQuery.assertion().id().eq(oasisAssertion.getId());
        assertionLinkMbvQuery.mbvService().id().eq(mbvService.getId());
        AssertionLinkedToMBV link = assertionLinkMbvQuery.getUniqueResult();

        return link != null;
    }

    private Integer getMbvServiceId(List<Integer> mbvServiceIdIn) {
        Integer mbvServiceId = mbvServiceIdIn.get(0);
        mbvServiceIdIn.remove(0);
        this.mbvServiceIds = mbvServiceIdIn;
        return mbvServiceId;
    }
}
