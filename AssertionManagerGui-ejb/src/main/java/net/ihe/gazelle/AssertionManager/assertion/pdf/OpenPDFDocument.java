package net.ihe.gazelle.AssertionManager.assertion.pdf;

import net.ihe.gazelle.common.filecache.FileCache;
import net.ihe.gazelle.common.filecache.FileCacheRenderer;
import net.ihe.gazelle.util.Md5Encryption;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Map;


@Name("openPDFDocument")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = OpenPDFDocument.TIMEOUT)
public class OpenPDFDocument implements Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(OpenPDFDocument.class);
    private static final long serialVersionUID = 3849914701380989881L;
    public static final int TIMEOUT = 10000;

    public void getIsStream() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        Map<String, String> params = externalContext.getRequestParameterMap();
        String documentUrl = params.get("url");
        if (documentUrl != null && !"".equals(documentUrl)) {
            try {
                FileCache.getFile("document" + Md5Encryption.hashPassword(documentUrl), documentUrl,
                        new FileCacheRendererImpl());
            } catch (Exception e) {
                LOG.error("Cannot open PDF");
            }
        }
    }

    private static class FileCacheRendererImpl implements FileCacheRenderer {

        public void render(OutputStream out, String value) throws NoSuchAlgorithmException, KeyManagementException {
            URL url = null;
            InputStream is = null;
            try {
                if (value.startsWith("https")) {
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, getTrustedCertificates(), new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    url = new URL(value);
                    HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                    is = con.getInputStream();

                } else {
                    url = new URL(value);
                    is = url.openStream();
                    url.openConnection();
                }
                IOUtils.copyLarge(is, out);

            } catch (IOException e) {
                LOG.error("Cannot open PDF: " + value);
                LOG.error(ExceptionUtils.getRootCauseMessage(e));
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        LOG.error("Cannot open PDF");
                    }
                }
            }
        }

        private TrustManager[] getTrustedCertificates() {
            return new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};
        }

        @Override
        public String getContentType() {
            return "application/pdf";
        }
    }
}


