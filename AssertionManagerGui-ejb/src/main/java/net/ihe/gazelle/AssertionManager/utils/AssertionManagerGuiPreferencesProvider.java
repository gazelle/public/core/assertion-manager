package net.ihe.gazelle.AssertionManager.utils;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.kohsuke.MetaInfServices;

import java.util.Date;

@MetaInfServices(PreferenceProvider.class)
public class AssertionManagerGuiPreferencesProvider implements PreferenceProvider {

    public static final int LIGHT = -100;

    @Override
    public Boolean getBoolean(String key) {
        String stringValue = ApplicationConfiguration.getValueOfVariable(key);
        try {
            return Boolean.valueOf(stringValue);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Date getDate(String key) {
        return null;
    }

    @Override
    public Integer getInteger(String key) {
        String stringValue = ApplicationConfiguration.getValueOfVariable(key);
        if (StringUtils.isNumeric(stringValue)) {
            return Integer.valueOf(stringValue);
        } else {
            return null;
        }
    }

    @Override
    public Object getObject(Object key) {
        return null;
    }

    @Override
    public String getString(String key) {
        return ApplicationConfiguration.getValueOfVariable(key);
    }

    @Override
    public void setBoolean(String key, Boolean arg1) {
    }

    @Override
    public void setDate(String key, Date arg1) {
    }

    @Override
    public void setInteger(String key, Integer arg1) {
    }

    @Override
    public void setObject(Object key, Object arg1) {
    }

    @Override
    public void setString(String key, String arg1) {
    }

    @Override
    public int compareTo(PreferenceProvider o) {
        int result;
        if (o.getWeight() != null) {
            result = getWeight().compareTo(o.getWeight());
        } else {
            result = getWeight();
        }
        return result;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof PreferenceProvider)) {
            return false;
        }
        PreferenceProvider o = (PreferenceProvider) obj;
        if (!getWeight().equals(o.getWeight())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return new HashCodeBuilder().append(getWeight()).toHashCode();
    }

    @Override
    public Integer getWeight() {
        return LIGHT;
    }

}