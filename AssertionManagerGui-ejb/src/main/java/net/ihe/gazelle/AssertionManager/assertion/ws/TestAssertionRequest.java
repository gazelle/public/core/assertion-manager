package net.ihe.gazelle.AssertionManager.assertion.ws;

import net.ihe.gazelle.AssertionManager.assertion.pdf.DocumentClient;
import net.ihe.gazelle.AssertionManager.assertion.xml.response.DocumentResponse;
import net.ihe.gazelle.AssertionManager.assertion.xml.response.DocumentSectionResponse;
import net.ihe.gazelle.oasis.testassertion.OasisNormativeSource;
import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

public class TestAssertionRequest {

    private static final Logger LOG = LoggerFactory.getLogger(TestAssertionRequest.class);

    private String assertionId;
    private String idScheme;
    private List<OasisTestAssertion> assertions;
    private Response response;
    private EntityManager entityManager;

    public TestAssertionRequest(EntityManager entityManagerIn) {
        this.entityManager = entityManagerIn;
    }

    public TestAssertionRequest(String assertionIdIn, String idSchemeIn, EntityManager entityManagerIn) {
        this.assertionId = assertionIdIn;
        this.idScheme = idSchemeIn;
        this.entityManager = entityManagerIn;
    }

    public Response getResponse() {
        return this.response;
    }

    public void getTestAssertionByIdAndIdScheme() throws JAXBException, UnsupportedEncodingException {
        LOG.info("start getTestAssertionByIdAndIdScheme");
        getTestAssertions();
        getAssertionResponse();

        LOG.info("end getTestAssertionByIdAndIdScheme");
    }

    public void getTestAssertionDocument() throws JAXBException {
        LOG.info("start getTestAssertionDocument");

        getTestAssertions();
        getDocumentResponse();

        LOG.info("end getTestAssertionDocument");
    }

    public void getTestAssertionDocumentSection() throws JAXBException {
        LOG.info("start getTestAssertionDocumentSection");

        getTestAssertions();
        getDocumentSectionResponse();

        LOG.info("end getTestAssertionDocumentSection");
    }

    public void openTestAssertionDocument() throws JAXBException {
        LOG.info("start openTestAssertionDocument");

        getTestAssertions();
        redirect(formatDocumentUri());

        LOG.info("end openTestAssertionDocument");
    }

    public void openIdSchemeDocumentPage(String idSchemeIn, int page) {
        redirect(formatDocumentPageUri(idSchemeIn, page));
    }

    public void openTestAssertionDocumentSection() throws JAXBException {
        LOG.info("start openTestAssertionDocumentSection");
        getTestAssertions();
        redirect(formatDocumentSectionUri());

        LOG.info("end openTestAssertionDocumentSection");
    }

    private void checkParameters() {
        response = Parameters.checkParameters(assertionId, idScheme);
    }

    private Response checkTestAssertionsAreFound() {
        Response localResponse = null;
        if (!isThereAnyElementIn(assertions)) {
            if (OasisTarget.exists(idScheme, entityManager)) {
                localResponse = Response
                        .status(Response.Status.NOT_FOUND)
                        .header("Warning",
                                "Warning: 001 AssertionManager \"NAV: no test assertion for id=" + assertionId
                                        + " , idScheme=" + idScheme + "\"").build();
            } else {
                localResponse = Response
                        .status(Response.Status.NOT_FOUND)
                        .header("Warning",
                                "Warning: 002 AssertionManager \"NAV: unknown idScheme " + idScheme
                                        + ", please use /idSchemes to get the list of possible idScheme\"").build();
            }
        }
        return localResponse;
    }

    private GenericEntity<DocumentSectionResponse> formatDocumentSection() {
        String assertionUri = getTestAssertionNormativeSourceUri();

        String pageNumber = assertions.get(0).getTestAssertionPageNumber();

        DocumentSectionResponse resultString = new DocumentSectionResponse(assertionUri + "#page=" + pageNumber);

        GenericEntity<DocumentSectionResponse> entity = new GenericEntity<DocumentSectionResponse>(resultString) {
        };
        return entity;
    }

    private String formatDocumentSectionUri() {
        String documentUrl = "";

        if (isThereAnyElementIn(assertions)) {
            String assertionUri = getTestAssertionNormativeSourceUri();
            String pageNumber = assertions.get(0).getTestAssertionPageNumber();

            documentUrl = DocumentClient.openPdf(assertionUri);
            documentUrl = documentUrl + "#page=" + pageNumber;

        }
        return documentUrl;
    }

    private String formatDocumentPageUri(String idSchemeIn, int page) {
        String assertionUri = OasisNormativeSource.getIdSchemeNormativeSourceUri(idSchemeIn);
        return DocumentClient.openPdf(assertionUri) + "#page=" + page;
    }


    private String formatDocumentUri() {
        String documentUrl = "";
        if (isThereAnyElementIn(assertions)) {
            String assertionUri = getTestAssertionNormativeSourceUri();
            documentUrl = DocumentClient.openPdf(assertionUri) + "#page=0";
        }
        return documentUrl;
    }

    private void getAssertionResponse() throws UnsupportedEncodingException {
        if (response == null) {
            OasisTestAssertionWrapper wrapper = new OasisTestAssertionWrapper(assertions);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                JAXBContext jc = JAXBContext.newInstance(OasisTestAssertionWrapper.class);

                Marshaller marshaller = jc.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller
                        .setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
                                "http://docs.oasis-open.org/ns/tag/taml-201002/ http://gazelle.ihe.net/XSD/OASIS/TAML/testAssertionMarkupLanguage.xsd");
                marshaller.marshal(wrapper, baos);
            } catch (PropertyException e) {
                LOG.error(e.getMessage());
            } catch (JAXBException e) {
                LOG.error(e.getMessage());
            }

            String responseAsString = baos.toString("UTF-8");
            responseAsString = responseAsString.replaceFirst("xmlns:ns2", "xmlns");
            response = Response.ok(responseAsString).build();
        }
    }

    private void getDocumentResponse() {
        if (response == null) {

            DocumentResponse resultString = new DocumentResponse(getTestAssertionNormativeSourceUri());

            GenericEntity<DocumentResponse> entity = new GenericEntity<DocumentResponse>(resultString) {
            };
            response = Response.ok(entity).build();
        }
    }

    private void getDocumentSectionResponse() {
        if (response == null) {
            GenericEntity<DocumentSectionResponse> entity = formatDocumentSection();
            response = Response.ok(entity).build();
        }
    }

    private String getTestAssertionNormativeSourceUri() {
        return assertions.get(0).getNormativeSource().getRefSourceItems().get(0).getUri();
    }

    private void getTestAssertions() throws JAXBException {
        checkParameters();
        if (response == null) {
            assertions = OasisTestAssertion.getTestAssertion(assertionId, idScheme, entityManager);
            response = checkTestAssertionsAreFound();
        }
    }

    public List<OasisTestAssertion> getAssertions() {
        return assertions;
    }

    private boolean isThereAnyElementIn(List<OasisTestAssertion> result) {
        return result.get(0) != null;
    }

    private void redirect(String result) {
        if (response == null) {
            HttpServletResponse httpResponse = ResteasyProviderFactory.getContextData(HttpServletResponse.class);
            try {
                httpResponse.sendRedirect(result);
            } catch (IOException e) {
                LOG.error(Arrays.toString(e.getStackTrace()));
            }
        }
    }

}
