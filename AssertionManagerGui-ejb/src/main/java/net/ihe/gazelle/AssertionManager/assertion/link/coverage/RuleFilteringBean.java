package net.ihe.gazelle.AssertionManager.assertion.link.coverage;

import net.ihe.gazelle.tf.ws.data.RuleWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.richfaces.model.Filter;

import java.io.Serializable;

@Name("ruleFilteringBean")
@Scope(ScopeType.PAGE)
public class RuleFilteringBean implements Serializable {


    private static final long serialVersionUID = -8464581459842726512L;

    private String filterRules = "";

    public Filter<?> filterRules() {
        return new Filter<RuleWrapper>() {

            @Override
            public boolean accept(RuleWrapper ruleWrapper) {
                return filterRules.length() == 0 || ruleWrapper.getName().toLowerCase().contains(filterRules.toLowerCase());
            }
        };
    }

    public RuleFilteringBean() {
    }

    public String getFilterValue() {
        return filterRules;
    }

    public void setFilterValue(String filterValue) {
        this.filterRules = filterValue;
    }
}
