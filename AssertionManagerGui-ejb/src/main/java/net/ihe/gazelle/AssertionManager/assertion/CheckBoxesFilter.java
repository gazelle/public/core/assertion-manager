package net.ihe.gazelle.AssertionManager.assertion;

import org.jboss.seam.annotations.Name;

@Name("checkBoxesFilter")
public enum CheckBoxesFilter {
    ANY("Any", ""),
    TEST("Test", ""),
    DEPRECATED_TEST("Deprecated Test", ""),
    RULES("Rules", ""),
    MBV("Mbv", ""),
    ACTOR("Actor", ""),
    TRANSACTION("Transaction", ""),
    AIPO("AIPO", ""),
    STANDARD("Standard", ""),
    INTEGRATION_PROFILE("Integration profile", "");

    private String friendlyName;
    private String labelToDisplay;

    CheckBoxesFilter(String friendlyNameIn, String labelToDisplayIn) {
        this.friendlyName = friendlyNameIn;
        this.setLabelToDisplay(labelToDisplayIn);
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setLabelToDisplay(String labelToDisplayIn) {
        this.labelToDisplay = labelToDisplayIn;
    }

    public String getLabelToDisplay() {
        return labelToDisplay;
    }

}
