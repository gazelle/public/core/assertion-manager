package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Name("uploadManager")
@Stateless
@GenerateInterface(value = "UploadManagerLocal")
public class UploadManager implements UploadManagerLocal {

    private static final Logger LOG = LoggerFactory.getLogger(UploadManager.class);

    private List<File> files = new ArrayList<File>();
    private boolean autoUpload = false;
    private boolean useFlash = true;


    public int getSize() {
        if (getFiles().size() > 0) {
            return getFiles().size();
        } else {
            return 0;
        }
    }

    public void uploadListener(FileUploadEvent event) throws IOException {
        FacesMessages.instance().add("Starting to import assertions");

        FileOutputStream fos = null;
        try {
            File file = File.createTempFile("upload", "tmp");
            fos = new FileOutputStream(file);
            UploadedFile item = event.getUploadedFile();
            fos.write(item.getData());

            AssertionImporter.appendUploadHistory(event.getUploadedFile().getName() + " start import");
            String username = null;
            if (Identity.instance().isLoggedIn()){
                username = Identity.instance().getCredentials().getUsername();
                LOG.info("Logged in user: " + username);
            } else {
                LOG.warn("User not logged in");
            }
            QuartzDispatcher.instance().scheduleAsynchronousEvent("importAssertions", file, event.getUploadedFile().getName(), username);

        } catch (IOException e) {
            LOG.error(e.getMessage());
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
    }

    public String clearUploadData() {
        files.clear();
        return null;
    }

    public long getTimeStamp() {
        return System.currentTimeMillis();
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> filesIn) {
        this.files = filesIn;
    }


    public boolean isAutoUpload() {
        return autoUpload;
    }

    public void setAutoUpload(boolean autoUploadIn) {
        this.autoUpload = autoUploadIn;
    }

    public boolean isUseFlash() {
        return useFlash;
    }

    public void setUseFlash(boolean useFlashIn) {
        this.useFlash = useFlashIn;
    }

    public String currentUpload() throws IOException {
        return AssertionImporter.getCurrentUpload();
    }

    public String uploadHistory() throws IOException {
        return AssertionImporter.getUploadHistory();
    }

    public void clearHistory() throws IOException {
        AssertionImporter.clearHistory();
    }
}
