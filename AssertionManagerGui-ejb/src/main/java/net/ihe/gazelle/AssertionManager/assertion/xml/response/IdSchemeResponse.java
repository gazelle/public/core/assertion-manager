package net.ihe.gazelle.AssertionManager.assertion.xml.response;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "IdSchemes")
public class IdSchemeResponse extends JaxbString {

    public IdSchemeResponse() {
        super();
    }

    public IdSchemeResponse(String v) {
        this.setValue(v);
    }
}
