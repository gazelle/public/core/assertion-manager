package net.ihe.gazelle.AssertionManager.validator;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * <b>Class Description : </b>UriValidator<br>
 * <br>
 *
 * @author Jean-Francois Labbé / IHE-Europe development Project
 * @version 1.0 - 15/06/16
 * @class UriValidator
 * @package net.ihe.gazelle.AssertionManager.validator
 * @see jean-francois.labbe@ihe-europe.net - http://gazelle.ihe.net
 */
@Name("uriValidator")
@Scope(ScopeType.PAGE)
public class UriValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
                         Object value) throws ValidatorException {
        String uri = String.valueOf(value);
        try {
            new URL(uri);
        } catch (MalformedURLException e) {
            FacesMessage message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Invalid url",
                    "The url you entered is not valid.");
            throw new ValidatorException(message,e);
        }
    }
}
