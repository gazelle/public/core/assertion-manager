package net.ihe.gazelle.AssertionManager.idScheme;

import net.ihe.gazelle.oasis.testassertion.OasisTarget;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IdSchemesCoverage implements Comparable<IdSchemesCoverage>, Serializable {

    private static final int HUNDRED_PERCENT = 100;
    private static final long serialVersionUID = -378859351481403184L;
    private int id;
    private String idScheme = "";
    private String description = "";
    private String scope = "";
    private int nbAssertionsCovered = 0;
    private int nbAssertionsNotCovered = 0;
    private int nbAssertionsForThisIdScheme = 0;
    private int nbAssertionsCoveredThroughTests = 0;
    private int nbAssertionsCoveredThroughRules = 0;
    private int nbAssertionsCoveredThroughMbv = 0;
    private int nbAssertionsNotTestable = 0;
    private int nbAssertionsTestable = 0;
    private int percentCovered = 0;
    private int percentNotCovered = 0;
    /*
     * Field used to find the last row of the datatable
     * */
    private boolean displayActions = true;

    public IdSchemesCoverage(String idSchemeIn, String scopeIn, int idIn) {
        super();
        this.idScheme = idSchemeIn;
        this.scope = scopeIn;
        this.id = idIn;
    }

    public IdSchemesCoverage(String idSchemeIn) {
        super();
        this.idScheme = idSchemeIn;
        this.id = 0;
    }

    public static List<IdSchemesCoverage> compute(List<OasisTarget> idSchemes, String selectedScopeForFilter) {

        List<IdSchemesCoverage> idSchemesCoverage = new ArrayList<IdSchemesCoverage>();
        for (OasisTarget oasisTarget : idSchemes) {
            IdSchemesCoverage cov = new IdSchemesCoverage(oasisTarget.getIdScheme(), selectedScopeForFilter,
                    oasisTarget.getId());
            cov.compute();
            cov.setDescription(oasisTarget.getContent());
            idSchemesCoverage.add(cov);
        }
        Collections.sort(idSchemesCoverage);
        computeTotals(idSchemesCoverage);
        return idSchemesCoverage;
    }

    private static void computeTotals(List<IdSchemesCoverage> idSchemesCoverages) {
        IdSchemesCoverage idSchemesCoverageTotal = new IdSchemesCoverage("Total");
        int nbAssertionsCovered = 0;
        int nbAssertionsNotCovered = 0;
        int nbAssertionsForThisIdScheme = 0;
        int nbAssertionsCoveredThroughTests = 0;
        int nbAssertionsCoveredThroughRules = 0;
        int nbAssertionsCoveredThroughMbv = 0;
        int nbAssertionsNotTestable = 0;
        for (IdSchemesCoverage idSchemesCoverage : idSchemesCoverages) {
            nbAssertionsCovered += idSchemesCoverage.getNbAssertionsCovered();
            nbAssertionsNotCovered += idSchemesCoverage.getNbAssertionsNotCovered();
            nbAssertionsForThisIdScheme += idSchemesCoverage.getNbAssertionsForThisIdScheme();
            nbAssertionsCoveredThroughTests += idSchemesCoverage.getNbAssertionsCoveredThroughTests();
            nbAssertionsCoveredThroughRules += idSchemesCoverage.getNbAssertionsCoveredThroughRules();
            nbAssertionsCoveredThroughMbv += idSchemesCoverage.getNbAssertionsCoveredThroughMbv();
            nbAssertionsNotTestable += idSchemesCoverage.getNbAssertionsNotTestable();
        }

        idSchemesCoverageTotal.setNbAssertionsCovered(nbAssertionsCovered);
        idSchemesCoverageTotal.setNbAssertionsNotCovered(nbAssertionsNotCovered);
        idSchemesCoverageTotal.setNbAssertionsForThisIdScheme(nbAssertionsForThisIdScheme);
        idSchemesCoverageTotal.setNbAssertionsCoveredThroughTests(nbAssertionsCoveredThroughTests);
        idSchemesCoverageTotal.setNbAssertionsCoveredThroughRules(nbAssertionsCoveredThroughRules);
        idSchemesCoverageTotal.setNbAssertionsCoveredThroughMbv(nbAssertionsCoveredThroughMbv);
        idSchemesCoverageTotal.setNbAssertionsNotTestable(nbAssertionsNotTestable);
        idSchemesCoverageTotal.computePercentCovered();
        idSchemesCoverageTotal.computePercentNotCovered();
        idSchemesCoverageTotal.setDisplayActions(false);
        idSchemesCoverages.add(idSchemesCoverageTotal);
    }

    public void compute() {
        computeAssertionNotTestable();
        computeTotalAssertions();
        computeAssertionTestable();
        computeTotalCoveredAssertions();
        computeAssertionCoveredThroughTests();
        computeAssertionCoveredThroughRules();
        computeAssertionCoveredThroughMbv();
        computePercentCovered();
        computePercentNotCovered();
    }

    public int getNbAssertionsTestable() {
        return nbAssertionsTestable;
    }

    private void computeAssertionTestable() {
        nbAssertionsTestable = getNbAssertionsForThisIdScheme() - getNbAssertionsNotTestable();
    }

    public String getIdScheme() {
        return idScheme;
    }

    public void setIdScheme(String idScheme) {
        this.idScheme = idScheme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNbAssertionsCovered() {
        return nbAssertionsCovered;
    }

    public void setNbAssertionsCovered(int nbAssertionsCoveredIn) {
        this.nbAssertionsCovered = nbAssertionsCoveredIn;
    }

    public int getNbAssertionsForThisIdScheme() {
        return nbAssertionsForThisIdScheme;
    }

    public void setNbAssertionsForThisIdScheme(int nbAssertionsForThisIdSchemeIn) {
        this.nbAssertionsForThisIdScheme = nbAssertionsForThisIdSchemeIn;
    }

    public int getPercentCovered() {
        return percentCovered;
    }

    public void setPercentCovered(int percentCoveredIn) {
        this.percentCovered = percentCoveredIn;
    }

    public int getPercentNotCovered() {
        return percentNotCovered;
    }

    public void setPercentNotCovered(int percentNotCoveredIn) {
        this.percentNotCovered = percentNotCoveredIn;
    }

    public int getNbAssertionsNotCovered() {
        return nbAssertionsNotCovered;
    }

    public void setNbAssertionsNotCovered(int nbAssertionsNotCoveredIn) {
        this.nbAssertionsNotCovered = nbAssertionsNotCoveredIn;
    }

    public int getNbAssertionsCoveredThroughTests() {
        return nbAssertionsCoveredThroughTests;
    }

    public void setNbAssertionsCoveredThroughTests(int nbAssertionsCoveredThroughTestsIn) {
        this.nbAssertionsCoveredThroughTests = nbAssertionsCoveredThroughTestsIn;
    }

    public int getNbAssertionsCoveredThroughRules() {
        return nbAssertionsCoveredThroughRules;
    }

    public void setNbAssertionsCoveredThroughRules(int nbAssertionsCoveredThroughRulesIn) {
        this.nbAssertionsCoveredThroughRules = nbAssertionsCoveredThroughRulesIn;
    }

    protected void computePercentCovered() {
        if (getNbAssertionsTestable() == 0) {
            percentCovered = HUNDRED_PERCENT;
        } else {
            percentCovered = (HUNDRED_PERCENT * nbAssertionsCovered) / getNbAssertionsTestable();
        }
    }

    protected void computePercentNotCovered() {
        percentNotCovered = HUNDRED_PERCENT - percentCovered;
    }

    private void computeTotalCoveredAssertions() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(idScheme, scope);
        nbAssertionsCovered = query.countCoveredAssertions();
        nbAssertionsNotCovered = getNbAssertionsTestable() - nbAssertionsCovered;
    }


    private void computeTotalAssertions() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(idScheme, scope);
        nbAssertionsForThisIdScheme = query.getCount();
    }

    private void computeAssertionCoveredThroughTests() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(idScheme, scope);
        nbAssertionsCoveredThroughTests = query.countAssertionCoveredThroughTests();
    }


    private void computeAssertionCoveredThroughRules() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(idScheme, scope);
        nbAssertionsCoveredThroughRules = query.countAssertionCoveredThroughRules();
    }

    private void computeAssertionCoveredThroughMbv() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(idScheme, scope);
        nbAssertionsCoveredThroughMbv = query.countAssertionCoveredThroughMbv();
    }

    private void computeAssertionNotTestable() {
        OasisTestAssertionCoverageQueryHelper query = new OasisTestAssertionCoverageQueryHelper(idScheme, scope);
        nbAssertionsNotTestable = query.countNotTestableAssertions();
    }

    @Override
    public int compareTo(IdSchemesCoverage o) {
        if (o == null) {
            return 1;
        }
        if (this.idScheme == null) {
            return -1;
        }
        return this.idScheme.toLowerCase().compareTo(o.idScheme.toLowerCase());
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof IdSchemesCoverage)) {
            return false;
        }
        IdSchemesCoverage o = (IdSchemesCoverage) obj;
        if (id != o.id) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }

    public boolean isDisplayActions() {
        return displayActions;
    }

    public void setDisplayActions(boolean displayActionsIn) {
        this.displayActions = displayActionsIn;
    }

    public int getNbAssertionsCoveredThroughMbv() {
        return nbAssertionsCoveredThroughMbv;
    }

    public void setNbAssertionsCoveredThroughMbv(int nbAssertionsCoveredThroughMbvIn) {
        this.nbAssertionsCoveredThroughMbv = nbAssertionsCoveredThroughMbvIn;
    }

    public int getId() {
        return id;
    }

    public void setId(int idIn) {
        this.id = idIn;
    }

    public int getNbAssertionsNotTestable() {
        return nbAssertionsNotTestable;
    }

    public void setNbAssertionsNotTestable(int nbAssertionsNotTestable) {
        this.nbAssertionsNotTestable = nbAssertionsNotTestable;
    }
}
