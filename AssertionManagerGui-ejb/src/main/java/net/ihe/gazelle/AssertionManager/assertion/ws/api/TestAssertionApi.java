package net.ihe.gazelle.AssertionManager.assertion.ws.api;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import net.ihe.gazelle.AssertionManager.assertion.xml.response.IdSchemeResponse;
import net.ihe.gazelle.assertion.ws.data.WsAssertionWrapper;
import net.ihe.gazelle.assertion.ws.data.WsIdSchemeWrapper;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ejb.Local;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Local
@Path("/testAssertion")
@Api("/testAssertion")
public interface TestAssertionApi {

    String THE_ASSERTION_ID = "The assertion id ";
    String THE_SCHEME_ID = "The Scheme id ";
    String JAVA_UTIL_LIST = "java.util.List";
    String TEXT_XML = "text/xml";
    String ASSERTION_ID = "assertionId";
    String ID_SCHEME = "idScheme";
    String KEYWORD = "keyword";

    @GET
    @Path("/assertions")
    @Produces(TEXT_XML)
    @ApiOperation(value = "list all assertions", notes = "list all assertions", responseClass = JAVA_UTIL_LIST)
    @Wrapped
    List<OasisTestAssertion> getTestAssertions() throws JAXBException;

    @GET
    @Path("/assertion")
    @Produces(TEXT_XML)
    @ApiOperation(value = "Get an assertion", notes = "Given its assertion id and its id scheme or all assertions in one id scheme", responseClass = JAVA_UTIL_LIST)
    @Wrapped
    Response getTestAssertionByIdAndIdScheme(@ApiParam(value = THE_ASSERTION_ID, required = false) @QueryParam(ASSERTION_ID) String assertionId,
                                             @ApiParam(value = THE_SCHEME_ID, required = true) @QueryParam(ID_SCHEME) String idScheme,
                                             @ApiParam(value = "Specify asked format can be: xml or nothing ", required = false) @QueryParam("format") String format) throws JAXBException, UnsupportedEncodingException;

    @GET
    @Path("/assertionSimple")
    @Produces(TEXT_XML)
    @ApiOperation(value = "Get an assertion", notes = "Given its assertion id and its id scheme or all assertions in one id scheme", responseClass = JAVA_UTIL_LIST)
    @Wrapped
    Response getTestAssertionByIdAndIdSchemeWithoutStyle(@ApiParam(value = THE_ASSERTION_ID, required = false) @QueryParam(ASSERTION_ID) String assertionId,
                                                         @ApiParam(value = THE_SCHEME_ID, required = true) @QueryParam(ID_SCHEME) String idScheme) throws JAXBException, UnsupportedEncodingException;

    @GET
    @Path("/idSchemes")
    @ApiOperation(value = "List all available id schemes", responseClass = JAVA_UTIL_LIST)
    @Produces(TEXT_XML)
    List<IdSchemeResponse> getIdSchemes() throws JAXBException;

    @GET
    @Path("/assertion/document")
    @Produces(TEXT_XML)
    @ApiOperation(value = "Get the document related to an assertion", responseClass = JAVA_UTIL_LIST)
    @Wrapped
    Response getTestAssertionDocument(@ApiParam(value = THE_ASSERTION_ID, required = false) @QueryParam(ASSERTION_ID) String assertionId,
                                      @ApiParam(value = THE_SCHEME_ID, required = true) @QueryParam(ID_SCHEME) String idScheme) throws JAXBException;

    @GET
    @Path("/assertion/documentSection")
    @ApiOperation(value = "Get the document section related to an assertion", notes = "If the assertion id is not provided then the assertion document is returned", responseClass = JAVA_UTIL_LIST)
    @Produces(TEXT_XML)
    Response getTestAssertionDocumentSection(@ApiParam(value = THE_ASSERTION_ID, required = false) @QueryParam(ASSERTION_ID) String assertionId,
                                             @ApiParam(value = THE_SCHEME_ID, required = true) @QueryParam(ID_SCHEME) String idScheme) throws JAXBException;

    @GET
    @Path("/assertion/document/open")
    @ApiOperation(value = "Open the document with pdfjs", notes = "It will fail with the rest api browser, please use your browser", responseClass = "void")
    void openTestAssertionDocument(@ApiParam(value = THE_ASSERTION_ID, required = false) @QueryParam(ASSERTION_ID) String assertionId,
                                   @ApiParam(value = THE_SCHEME_ID, required = true) @QueryParam(ID_SCHEME) String idScheme) throws JAXBException;

    @GET
    @Path("/assertion/document/openPage")
    @ApiOperation(value = "Open the document with pdfjs at the given page", notes = "It will fail with the rest api browser, please use your browser", responseClass = "void")
    void openIdSchemeDocumentPage(@ApiParam(value = THE_SCHEME_ID, required = true) @QueryParam(ID_SCHEME) String idScheme,
                                  @ApiParam(value = "Page number ", required = false) @QueryParam("page") int page) throws JAXBException;

    @GET
    @Path("/assertion/documentSection/open")
    @ApiOperation(value = "Open the document section with pdfjs", notes = "It will fail with the rest api browser, please use your browser", responseClass = "void")
    void openTestAssertionDocumentSection(@ApiParam(value = THE_ASSERTION_ID, required = false) @QueryParam(ASSERTION_ID) String assertionId,
                                          @ApiParam(value = THE_SCHEME_ID, required = true) @QueryParam(ID_SCHEME) String idScheme) throws JAXBException;

    @GET
    @Path("/v2/idSchemes")
    @Produces(TEXT_XML)
    WsIdSchemeWrapper getIdSchemesV2() throws JAXBException;

    @GET
    @Path("/v2/idSchemes/{idSchemeId}/assertions")
    @Produces(TEXT_XML)
    WsAssertionWrapper getAssertionsForSchemeV2(@PathParam("idSchemeId") String idSchemeId) throws JAXBException;

    @GET
    @Path("/v2/idSchemes/{idSchemeId}/assertions/{assertionId}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getAssertionForSchemeV2(@PathParam("idSchemeId") String idSchemeId, @PathParam(ASSERTION_ID) String assertionId) throws JAXBException;

    @GET
    @Path("/test/{testId}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getTestAssertions(@PathParam("testId") String testId) throws JAXBException;

    @GET
    @Path("/testStep/{id}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getTestStepAssertions(@PathParam("id") String testStepId) throws JAXBException;

    @GET
    @Path("/rule/{id}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getRuleAssertions(@PathParam("id") String ruleId) throws JAXBException;

    @GET
    @Path("/actor/{keyword}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getActorAssertions(@PathParam(KEYWORD) String keyword) throws JAXBException;

    @GET
    @Path("/transaction/{keyword}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getTransactionAssertions(@PathParam(KEYWORD) String keyword) throws JAXBException;

    @GET
    @Path("/profile/{keyword}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getProfileAssertions(@PathParam(KEYWORD) String keyword) throws JAXBException;

    @GET
    @Path("/aipo/{keyword}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getAIPOAssertions(@PathParam(KEYWORD) String keyword) throws JAXBException;

    @GET
    @Path("/standard/{keyword}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getStandardAssertions(@PathParam(KEYWORD) String keyword) throws JAXBException;

    @GET
    @Path("/auditMessage/{keyword}")
    @Produces(TEXT_XML)
    WsAssertionWrapper getAuditMessageAssertions(@PathParam(KEYWORD) String keyword) throws JAXBException;
}
