package net.ihe.gazelle.AssertionManager.assertion.link.appliesTo;

import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToStandard;
import net.ihe.gazelle.oasis.testassertion.AssertionAppliesToStandardQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tf.ws.data.StandardWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("appliesToStandardManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "AppliesToStandardManagerLocal")
public class AppliesToStandardManager extends AssertionLinkManager implements AppliesToStandardManagerLocal, Serializable {


    private static final long serialVersionUID = -4181671477580363694L;
    private StandardWrapper selectedStandard = new StandardWrapper();

    private GazelleListDataModel<StandardWrapper> standards;
    private String selectedIntegrationProfileOption;
    private String selectedActor;

    @Override
    public List<String> getDomainsAvailable() {
        return getTmWsClient().getDomainsAvailable();
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedStandardLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedStandardFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedStandardTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isStandardSelected();
    }

    public List<String> getIntegrationProfiles() {
        return getTmWsClient().getIntegrationProfilesNames(getSelectedDomain(), getSelectedActor());
    }

    public List<String> getIntegrationProfileOptions() {
        return getTmWsClient().getIntegrationProfileOptions(getSelectedDomain(), getSelectedActor(), getSelectedIntegrationProfileName());
    }

    public List<String> getActors() {
        return getTmWsClient().getActorsForFilter(getSelectedDomain(), getSelectedIntegrationProfileName());
    }

    public void setSelectedStandard(StandardWrapper standard) {
        selectedStandard = getFullStandard(standard);
    }

    /*Get full standard with description*/
    private StandardWrapper getFullStandard(StandardWrapper standard) {
        return getTmWsClient().getFullStandard(standard.getId());
    }

    public StandardWrapper getSelectedStandard() {
        return selectedStandard;
    }

    public GazelleListDataModel<StandardWrapper> getStandards() {
        if (standards == null) {
            standards = getTmWsClient().getStandards(getSelectedDomain(), getSelectedActor(), getSelectedIntegrationProfileName(), getSelectedIntegrationProfileOption());
        }
        return standards;
    }

    public boolean isStandardSelected() {
        return getSelectedStandard().getId() != 0;
    }

    public List<AssertionAppliesToStandard> assertionsOfSelectedStandard() {
        List<AssertionAppliesToStandard> listDistinct = new ArrayList<AssertionAppliesToStandard>();

        if (getSelectedStandard() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionAppliesToStandardQuery query = new AssertionAppliesToStandardQuery(entityManager);
            query.linkedEntityKeyword().eq(getSelectedStandard().getKeyword());
            listDistinct = query.getListDistinct();
        }
        return listDistinct;
    }

    public void unlinkSelectedStandardFrom(int assertionId) {
        if (getSelectedStandard() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            AssertionAppliesToStandardQuery query = new AssertionAppliesToStandardQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(getSelectedStandard().getKeyword());

            AssertionAppliesToStandard uniqueResult = query.getUniqueResult();

            entityManager.remove(uniqueResult);
            entityManager.flush();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void linkSelectedStandardTo(int assertionId) {
        if (getSelectedStandard() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            StandardWrapper standard = getSelectedStandard();
            AssertionAppliesToStandard link = new AssertionAppliesToStandard(standard.getKeyword(), standard.getName(), standard.getId(), standard.getBaseUrl(), standard.getPermanentLink(), assertion);
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public boolean isStandardCovered(String standardKeyword) {
        boolean result = false;
        AssertionAppliesToStandardQuery query = new AssertionAppliesToStandardQuery();
        query.linkedEntityKeyword().eq(standardKeyword);
        List<AssertionAppliesToStandard> listDistinct = query.getListDistinct();
        if ((listDistinct != null) && (listDistinct.size() != 0)) {
            result = true;
        }
        return result;
    }

    public boolean isSelectedStandardLinkedTo(int assertionId) {
        boolean result = false;
        AssertionAppliesToStandardQuery query = new AssertionAppliesToStandardQuery();
        query.assertion().id().eq(assertionId);
        if (getSelectedStandard() != null) {
            query.linkedEntityKeyword().eq(getSelectedStandard().getKeyword());
        }
        AssertionAppliesToStandard uniqueResult = query.getUniqueResult();
        if (uniqueResult != null) {
            result = true;
        }
        return result;
    }

    @Override
    public void selectedIntegrationProfileUpdate() {
        selectedStandard = new StandardWrapper();
        standards = null;
    }

    @Override
    public void selectedDomainUpdateCallback() {
        selectedActor = "";
        selectedIntegrationProfileOption = "";
        setSelectedIntegrationProfileName("");
        selectedStandard = new StandardWrapper();
        standards = null;
    }

    public String getSelectedIntegrationProfileOption() {
        return selectedIntegrationProfileOption;
    }

    public void setSelectedIntegrationProfileOption(String selectedIntegrationProfileOptionIn) {
        this.selectedIntegrationProfileOption = selectedIntegrationProfileOptionIn;
        standards = null;
    }

    public String getSelectedActor() {
        return selectedActor;
    }

    public void setSelectedActor(String selectedActorIn) {
        this.selectedActor = selectedActorIn;
        setSelectedIntegrationProfileName("");
    }
}
