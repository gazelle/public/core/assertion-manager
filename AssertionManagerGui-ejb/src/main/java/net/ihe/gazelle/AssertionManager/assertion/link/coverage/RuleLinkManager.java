package net.ihe.gazelle.AssertionManager.assertion.link.coverage;

import net.ihe.gazelle.AssertionManager.assertion.OasisTestAssertionFilter;
import net.ihe.gazelle.AssertionManager.assertion.link.AssertionLinkManager;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToRule;
import net.ihe.gazelle.oasis.testassertion.AssertionLinkedToRuleQuery;
import net.ihe.gazelle.oasis.testassertion.OasisTestAssertion;
import net.ihe.gazelle.tf.ws.data.RuleWrapper;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

@Name("ruleLinkManager")
@Scope(ScopeType.PAGE)
@GenerateInterface(value = "RuleLinkManagerLocal")
public class RuleLinkManager extends AssertionLinkManager implements RuleLinkManagerLocal, Serializable {


    private static final long serialVersionUID = -4088791934916766156L;
    private RuleWrapper rule;
    private List<String> integrationProfilesWithRules;
    private GazelleListDataModel<RuleWrapper> rules;

    public OasisTestAssertionFilter getFilter() {
     return  onlyTestableAssertion(super.getFilter());
    }

    @Override
    public void selectedIntegrationProfileUpdate() {
        resetSelectedRule();
        rules = null;
    }

    @Override
    public void selectedDomainUpdateCallback() {
        integrationProfilesWithRules = null;
        rules = null;
        resetSelectedRule();
    }

    public void resetSelectedRule() {
        this.rule = null;
        rules = null;
    }

    @Override
    public List<String> getDomainsAvailable() {
        return getTmWsClient().getDomainsWithRulesAvailable();
    }

    @Override
    public List<String> getIntegrationProfilesNames() {
        if (integrationProfilesWithRules == null) {
            integrationProfilesWithRules = getTmWsClient().getIntegrationProfilesNamesWithRules(getSelectedDomain());
        }
        return integrationProfilesWithRules;
    }

    @Override
    public boolean isSelectedTechnicalFrameworkTo(int assertionId) {
        return isSelectedRuleLinkedTo(assertionId);
    }

    @Override
    public void unlinkSelectedTechnicalFrameworkFrom(int assertionId) {
        unlinkSelectedRuleFrom(assertionId);
    }

    @Override
    public void linkSelectedTechnicalFrameworkTo(int assertionId) {
        linkSelectedRuleTo(assertionId);
    }

    @Override
    public boolean isTechnicalFrameworkSelected() {
        return isRuleSelected();
    }

    public GazelleListDataModel<RuleWrapper> getRules() {
        if (rules == null && !"".equals(getSelectedIntegrationProfileName())) {
            rules = new GazelleListDataModel<RuleWrapper>(getTmWsClient().getRulesOfIntegrationProfile(getSelectedIntegrationProfileName()));
        }
        return rules;
    }

    public void setSelectedRule(RuleWrapper ruleIn) {
        this.rule = ruleIn;
    }

    public boolean isSelectedRuleLinkedTo(int assertionId) {
        boolean result = false;
        if (getSelectedRule() != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            AssertionLinkedToRuleQuery query = new AssertionLinkedToRuleQuery(entityManager);
            query.assertion().id().eq(assertionId);
            query.linkedEntityKeyword().eq(String.valueOf(getSelectedRuleId()));
            AssertionLinkedToRule uniqueResult = query.getUniqueResult();
            if (uniqueResult != null) {
                result = true;
            }
        }
        return result;
    }

    public void linkSelectedRuleTo(int assertionId) {
        if (isRuleSelected()) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            OasisTestAssertion assertion = getAssertionFromId(assertionId);
            AssertionLinkedToRule link = new AssertionLinkedToRule(String.valueOf(getSelectedRuleId()), assertion, null, getSelectedRule().getPermanentLink(), getSelectedRule().getName());
            link.setAssertion(assertion);
            entityManager.merge(link);
            entityManager.flush();

            updateCoverage(entityManager, assertion.getId());
        }
    }

    public void unlinkSelectedRuleFrom(int assertionId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();

        AssertionLinkedToRuleQuery query = new AssertionLinkedToRuleQuery(entityManager);
        query.assertion().id().eq(assertionId);
        query.linkedEntityKeyword().eq(String.valueOf(getSelectedRuleId()));

        AssertionLinkedToRule uniqueResult = query.getUniqueResult();

        entityManager.remove(uniqueResult);
        entityManager.flush();
        OasisTestAssertion assertion = getAssertionFromId(assertionId);
        updateCoverage(entityManager, assertion.getId());
    }

    private void updateCoverage(EntityManager entityManager, int assertionId) {
        OasisTestAssertion assertion = entityManager.find(OasisTestAssertion.class, assertionId);

        assertion.updateCoverage();
        entityManager.merge(assertion);
        entityManager.flush();
    }

    public List<AssertionLinkedToRule> assertionsOfSelectedRule() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AssertionLinkedToRuleQuery query = new AssertionLinkedToRuleQuery(entityManager);
        query.linkedEntityKeyword().eq(String.valueOf(getSelectedRuleId()));
        return query.getListDistinct();
    }

    public RuleWrapper getSelectedRule() {
        return rule;
    }

    private String getSelectedRuleId() {
        return String.valueOf(getSelectedRule().getId());
    }

    public boolean isRuleSelected() {
        return getSelectedRule() != null;
    }

    public int getNbAssertionsForThisRule(int ruleId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AssertionLinkedToRuleQuery query = new AssertionLinkedToRuleQuery(entityManager);
        query.linkedEntityKeyword().eq(String.valueOf(ruleId));
        return query.getCount();
    }

    private String filterRules = "";

    public boolean filterRules(Object current) {
        RuleWrapper currentRule = (RuleWrapper) current;
        if (filterRules.length() == 0) {
            return true;
        }
        return currentRule.getName().toLowerCase().contains(filterRules.toLowerCase());
    }

    public String getFilterValue() {
        return filterRules;
    }

    public void setFilterValue(String filterValue) {
        this.filterRules = filterValue;
    }
}
