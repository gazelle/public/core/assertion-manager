INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), '100000000', 'upload_max_size');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'false', 'ip_login');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'),'.*','ip_login_admin');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/AssertionManagerGui', 'application_url');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/gazelle-documentation/assertion-manager/4.2.0.html', 'application_release_notes_url');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/gazelle-documentation/assertion-manager/user.html', 'documentation_url');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/gazelle/rest/', 'gazelle_test-management-url');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'true', 'cas_enabled');
INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'cgu link that need to be changed', 'link_to_cgu');