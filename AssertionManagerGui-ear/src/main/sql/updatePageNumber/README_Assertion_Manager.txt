README - Assertion Manager scheme update script utilization manual

This file intends to take you through the process of updating Assertion Manager's assertions by importing XML files, and modifying a few key elements in these files by using a script that will need to be altered.

Step 1 - Acquire the XML files
The XML files should be exported from gazelle.ihe.net's instance of Assertion Manager when new versions of one or several documents have been published, and the assertions have been updated by the Gazelle team. To do so, you need to be logged in to CAS, and head to the 'Id Schemes' tab of Assertion Manager.

Step 2 - Prepare the XML files before importing
Before you can import the extracted XML files to your instance of Assertion Manager, you need to match the referenced documents with their source on your database, so the imports will update the existing schemes instead of creating new ones.

To do so, open the 'assertionSchemeFileUpdate.sh' script in any text editor (vim, gedit, ...) and modify the following FOR EACH DOCUMENT :
(note1: all the information you need can be found in the 'Documents' tab of Assertion Manager)
(note2: to run the script, you need to have perl installed)

	- srcName 		: the document's name (found in the 'name' column)
	- uri 			: link to the source of the document (found in the 'url' column)
	- docId 		: document's ID (found in the 'documentId' column)
	- revId 		: document's revision number (found in the 'revisionId' column)
	- provenanceId 	: source of the document (found in the 'resource provenance' column)

Then, change which XML files you wish to update with the script by adding their file name in the 'for' loop. For example :

	for file in assertion-atna.xml assertion-ct.xml
	do
	...

You can add as many files as you wish, but be sure the schemes actually belong in the document by checking which are present in the 'Id schemes' column.

Before you run the script, make sure the .sh file and the .xml files are all in the same directory.

When you are certain the information you entered in the script is correct, save, then run it (in a terminal would be best, to be able to see potential errors).

Step 3 - Importing back into Assertion Manager
Once the XML files have been properly updated with the correct values, head to the 'Id Schemes' tab in Assertion Manager, and use the 'Import' button to add the files one at a time (again, you need to be logged in with CAS to access this feature). Once this is done, the assertions should point to the correct sections (or tables) and their associated pages.
